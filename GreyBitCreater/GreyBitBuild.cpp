#include "StdAfx.h"
#include "GreyBitBuild.h"
#include "CDib.h"

UINT GreyBitThreadProc(LPVOID lpParam);

GreyBitBuild::GreyBitBuild(void)
{
	m_nTotalNum = 0;
	m_nCurrNum  = 0;
	m_nSize     = 0;
	m_bBuilding = false;
	m_pThread   = NULL;
    m_pBuf      = 0;
	ResetInput();
}

GreyBitBuild::~GreyBitBuild(void)
{
    if(m_pBuf)
    {
        delete [] m_pBuf;
    }

	if(NULL != m_pThread)
	{
		delete m_pThread;
		m_pThread = NULL;
	}
}

void GreyBitBuild::Start(void)
{
	if(m_nTotalNum == 0 || m_nSize == 0)
	{
		return;
	}
	m_bBuilding = true;
	if(NULL != m_pThread)
	{
		delete m_pThread;
	}
    
    if(m_pBuf)
    {
        delete m_pBuf;
    }
    m_pBuf = new byte[m_nSize*m_nSize*3];
	m_pThread = AfxBeginThread(GreyBitThreadProc, this);
	m_pThread->m_bAutoDelete = false;
}

void GreyBitBuild::Stop(void)
{
	m_bBuilding = false;
	ResetInput();
}

bool GreyBitBuild::CheckInputFileAvaliable(int *pIdx)
{
	CFile myFile;

	for(int i=0; i<(CODE_ST_IDX+1); i++)
	{
		if(m_bInputSet[i] == true)
		{
			if ( m_InputFile[i].IsEmpty())
			{
				if(pIdx != NULL)
				{
					*pIdx = -1;
				}
				return false;
			}
            else
            {
                if(m_InputFile[i].Find(_T(".gvf")) > 0|| m_InputFile[i].Find(_T(".gbf"))>0)
                {
                    if(pIdx != NULL)
				    {
					    *pIdx = i;
				    }
                    return false;
                }
            }
			CFileStatus Status; 
			if(false == myFile.GetStatus(m_InputFile[i], Status))
			{
				if(pIdx != NULL)
				{
					*pIdx = i;
				}
				return false;
			}
		}
	}
	return true;
}

void GreyBitBuild::ResetInput(void)
{
	for(int i=0; i<(CODE_ST_IDX+1); i++)
	{
		m_bInputSet[i] = false;
	}
    memset(m_nCodeHoriOff, 0, sizeof(m_nCodeHoriOff));
}

void GreyBitBuild::UpdateInfo(void)
{
	if(m_CurrWnd)
	{
		m_CurrWnd->PostMessage(WM_MY_UPDATEINFO,0,0);
	}
}

#define ENABLE_OUT
UINT GreyBitThreadProc(LPVOID lpParam)
{
	GreyBitBuild *pGreyBitBuild = (GreyBitBuild *)lpParam;
	int nUpdateCount = pGreyBitBuild->m_nTotalNum/256;
	int nIdx;
	int nCode = 0;
	nUpdateCount = nUpdateCount>1?nUpdateCount:1;
	pGreyBitBuild->m_nCurrNum = 0;
	pGreyBitBuild->UpdateInfo();

	FreeTypeFontEngine cFreeType;
	cFreeType.SetFontSize(pGreyBitBuild->m_nSize, pGreyBitBuild->m_nScale);
#ifdef ENABLE_OUT
    GreyBitFontEngine  cGreyBitType;
    if(pGreyBitBuild->m_bVector)
    {
        pGreyBitBuild->m_nSize = 64;   // 最大值是127，这里设成100，以便于能够存储超出范围的点，不至于字形错乱
        pGreyBitBuild->m_bGrey = TRUE;
        pGreyBitBuild->m_bCompress = FALSE;
    }
    
    cGreyBitType.SetFile(pGreyBitBuild->m_OutputName.GetBuffer());
    cGreyBitType.PrepareCreate(pGreyBitBuild->m_nSize, pGreyBitBuild->m_bGrey?8:1, pGreyBitBuild->m_bCompress);
#endif
	while(1)
	{
		if(!pGreyBitBuild->m_bBuilding)
		{
			break;
		}

		nIdx = pGreyBitBuild->m_wAllCode[nCode];
		if(nIdx == 0)
		{
            nCode++;
			continue;
		}
		
		int   nWidth     = 0;
        int   nHoriOff   = 0;
		byte *pGlyphData = NULL;
		int   nDataLen   = 0;
        int   nBitCount  = 8;

		if(CODE_ST_BMP & nIdx)
		{
			// 从BMP文件中读取Glyph
            CFileFind myFileFind;
            CString   szFind;
            BOOL      bFind;

            szFind.Format(_T("%s\\*%x.bmp"), pGreyBitBuild->m_BmpPath, nCode);
		    bFind = myFileFind.FindFile(szFind);
            if(bFind)
            {
                CDib myDIB;
                CDib newDIB;
                bFind = myFileFind.FindNextFile();
                if(myDIB.Load(myFileFind.GetFilePath()))//GetFileName()))
                {
                    int nTrueHeight = pGreyBitBuild->m_nSize;
                    nBitCount = 8;
                    nWidth = myDIB.Width();
                    
                    if(nTrueHeight >= abs(myDIB.Height()))
                    {
                        newDIB.Create(nWidth, -nTrueHeight, nBitCount);
                        newDIB.BitBlt(0,(nTrueHeight-abs(myDIB.Height()))>>1,newDIB.Width(), nTrueHeight, myDIB, 0, 0);
                    }
                    else
                    {
                        nWidth = nWidth*nTrueHeight;
                        nWidth = nWidth/abs(myDIB.Height());
                        
                        newDIB.Create(nWidth, -nTrueHeight, nBitCount);
                        newDIB.StretchBlt(0, 0, newDIB.Width(), nTrueHeight, myDIB, 0, 0);
                    }

                    if(nWidth<= 3*nTrueHeight)
                    {
                        // 需要紧凑数据
                        int nPitch = newDIB.Pitch();
                        byte *pSrc = (byte *)newDIB.Bits();
                        byte *pDst = pGreyBitBuild->m_pBuf;
                        
                        if(newDIB.Height() > 0)
                        {
                            pSrc += (nTrueHeight-1)*nPitch;
                            nPitch = -nPitch;
                        }
                        for(int i=0; i<nTrueHeight; i++)
                        {
                            memcpy(pDst, pSrc, nWidth);
                            pDst += nWidth;
                            pSrc += nPitch;
                        }
                        pGlyphData = pGreyBitBuild->m_pBuf;
                        if(pGreyBitBuild->m_nCodeHoriOff[nCode] == MAX_CHAR_CODE)
                        {
                            nHoriOff = -nWidth;
                        }
                        else
                        {
                            nHoriOff = pGreyBitBuild->m_nCodeHoriOff[nCode];
                        }
                    }
                }
            }
		}
		else
		{
			// 从FreeType中读取Glyph
			int nFontIdx = CODE_ST_IDX & nIdx;
			cFreeType.SetFile(pGreyBitBuild->m_InputFile[nFontIdx].GetBuffer());
            cFreeType.SetFontSize(pGreyBitBuild->m_nSize, pGreyBitBuild->m_nScale);

			if(cFreeType.IsCodeAvaliable(nCode))
			{
				cFreeType.LoadGlyph(nCode);
				nDataLen   = cFreeType.GetBitsLen();
                if(pGreyBitBuild->m_bVector)
                {
                    pGlyphData = (byte *)cFreeType.GetOutline();
                }
                else
                {
				    pGlyphData = cFreeType.GetBits();
                }
				nWidth     = cFreeType.GetWidth();
                nHoriOff   = cFreeType.GetHoriOff();
                nBitCount  = 8;
			}
		}

		if(nWidth && pGlyphData)
		{
#ifdef ENABLE_OUT
            if(pGreyBitBuild->m_bVector)
            {
                cGreyBitType.SetOutline(nCode, pGlyphData, nWidth,nHoriOff);
            }
            else
            {
			    // 保存数据
                cGreyBitType.SetBits(nCode, pGlyphData, nWidth, nBitCount,nHoriOff);
            }
#endif
		}

		pGreyBitBuild->m_nCurrNum++;
		if(pGreyBitBuild->m_nCurrNum >= pGreyBitBuild->m_nTotalNum)
		{
			pGreyBitBuild->UpdateInfo();
			break;
		}

		if(pGreyBitBuild->m_nCurrNum%nUpdateCount == 0)
		{
			pGreyBitBuild->UpdateInfo();
		}
        nCode++;
	}
#ifdef ENABLE_OUT
    cGreyBitType.FinishCreate();
#endif
	pGreyBitBuild->Stop();
	pGreyBitBuild->UpdateInfo();
	return 0;
}
