// GreyBitCreaterDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "GreyBitCreater.h"
#include "GreyBitCreaterDlg.h"
#include "CDib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CGreyBitCreaterDlg 对话框




CGreyBitCreaterDlg::CGreyBitCreaterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGreyBitCreaterDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGreyBitCreaterDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_SIZE, m_SizeComboBox);
    DDX_Control(pDX, IDC_LIST_CHARSET, m_CodeListCtrl);
    DDX_Control(pDX, IDC_STATIC_VIEW, m_PreviewStatic);
    DDX_Control(pDX, IDC_STATIC_WIDTH, m_WidthStatic);
    DDX_Control(pDX, IDC_STATIC_HEIGHT, m_HeightStatic);
    DDX_Control(pDX, IDC_EDIT_CODE, m_CodeEdit);
    DDX_Control(pDX, IDC_SLIDER_CODE, m_CodeSlider);
    DDX_Control(pDX, IDC_PROGRESS_ALL, m_ProgressBar);
    DDX_Control(pDX, IDC_STATIC_PROGRESS_PER, m_PercentStatic);
    DDX_Control(pDX, IDC_STATIC_PROGRESS_NUM, m_TotalStatic);
    DDX_Control(pDX, IDC_CHECK_CHARSET_BMP, m_BmpCheck);
    DDX_Control(pDX, IDC_CHECK_CHARSET_TXT, m_TxtCheck);
    DDX_Control(pDX, IDC_CHECK_CHARSET_ALL, m_SelectAllCheck);
    DDX_Control(pDX, IDC_RADIO_GREY, m_GreyRadio);
    DDX_Control(pDX, IDC_RADIO_COMPRESS, m_CompressRadio);
    DDX_Control(pDX, IDC_COMBO_INPUT, m_InputFileCombo);
    DDX_Control(pDX, IDC_COMBO_INPUT_BMP, m_InputBmpCombo);
    DDX_Control(pDX, IDC_COMBO_INPUT_TXT, m_InputTxtCombo);
    DDX_Control(pDX, IDC_COMBO_OUTPUT, m_OutputPathCombo);
    DDX_Control(pDX, IDC_BUILD, m_BuildButton);
    DDX_Control(pDX, IDC_RADIO_VECT, m_VectorRadio);
    DDX_Control(pDX, IDC_CHECK_OUTLINE, m_OutlineCheck);
    DDX_Control(pDX, IDC_EDIT_SCALE, m_ScaleEdit);
    DDX_Control(pDX, IDC_STATIC_HOFF, m_HOffStatic);
}

BEGIN_MESSAGE_MAP(CGreyBitCreaterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_MY_UPDATEINFO, OnMyUpdateInfo) 
    ON_MESSAGE(WM_MY_SHOWCTRL, OnMyShowCtrl) 
    ON_MESSAGE(WM_ITEM_CHANGED,OnItemChanged)
	ON_BN_CLICKED(IDC_BUTTON_INPUT_OPEN, &CGreyBitCreaterDlg::OnBnClickedButtonInputOpen)
	ON_BN_CLICKED(IDC_BUTTON_INPUT_TXT, &CGreyBitCreaterDlg::OnBnClickedButtonInputTxt)
	ON_BN_CLICKED(IDC_BUTTON_INPUT_BMP, &CGreyBitCreaterDlg::OnBnClickedButtonInputBmp)
	ON_BN_CLICKED(IDC_BUTTON_OUTPUT_OPEN, &CGreyBitCreaterDlg::OnBnClickedButtonOutputOpen)
	ON_BN_CLICKED(IDC_BUILD, &CGreyBitCreaterDlg::OnBnClickedBuild)
	ON_BN_CLICKED(IDC_CHECK_CHARSET_ALL, &CGreyBitCreaterDlg::OnBnClickedCheckCharsetAll)
	ON_CBN_EDITCHANGE(IDC_COMBO_SIZE, &CGreyBitCreaterDlg::OnCbnEditchangeComboSize)
	ON_EN_CHANGE(IDC_EDIT_CODE, &CGreyBitCreaterDlg::OnEnChangeEditCode)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BUTTON_GOTO, &CGreyBitCreaterDlg::OnBnClickedButtonGoto)
    ON_CBN_CLOSEUP(IDC_COMBO_INPUT, &CGreyBitCreaterDlg::OnCbnCloseupComboInput)
    ON_CBN_DROPDOWN(IDC_COMBO_INPUT, &CGreyBitCreaterDlg::OnCbnDropdownComboInput)
    ON_BN_CLICKED(IDC_RADIO_VECT, &CGreyBitCreaterDlg::OnBnClickedRadioVect)
    ON_BN_CLICKED(IDC_CHECK_OUTLINE, &CGreyBitCreaterDlg::OnBnClickedCheckOutline)
    ON_BN_CLICKED(IDC_RADIO_BITMAP, &CGreyBitCreaterDlg::OnBnClickedRadioBitmap)
//    ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CHARSET, &CGreyBitCreaterDlg::OnLvnItemchangedListCharset)
ON_BN_CLICKED(IDC_RADIO_GREY, &CGreyBitCreaterDlg::OnBnClickedRadioGrey)
ON_BN_CLICKED(IDC_RADIO_MONO, &CGreyBitCreaterDlg::OnBnClickedRadioMono)
ON_BN_CLICKED(IDC_BUTTON_SAVE, &CGreyBitCreaterDlg::OnBnClickedButtonSave)
ON_BN_CLICKED(IDC_BUTTON_GCF, &CGreyBitCreaterDlg::OnBnClickedButtonGcf)
END_MESSAGE_MAP()


// CGreyBitCreaterDlg 消息处理程序

BOOL CGreyBitCreaterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_GreyRadio.SetCheck(TRUE);
	m_CompressRadio.SetCheck(TRUE);
    m_VectorRadio.SetCheck(TRUE);
	
	InitInput();
	InitCharset();
	InitOutput();
	InitPreview();
	InitBuild();
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CGreyBitCreaterDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CGreyBitCreaterDlg::OnCancel()
{
	if(IsBuilding())
	{
		CString szTitle;
		CString szContent;

		szTitle.LoadString(IDS_TITLE_WARNING);
		szContent.LoadString(IDS_MSG_INPROCESSING);
		MessageBox(szContent,szTitle,MB_OK);
		return;
	}

    for(int i=0; i<= CODE_ST_IDX; i++)
    {
        if(m_pViewer[i])
        {
            m_GreyBitFactory.DestroyViewer(m_pViewer[i]);
        }
    }
        
	CDialog::OnCancel();
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CGreyBitCreaterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CGreyBitCreaterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// Init
void CGreyBitCreaterDlg::InitInput(void)
{
    m_InputFileNum = 0;
    for(int i=0; i<= CODE_ST_IDX; i++)
    {
        m_pViewer[i] = NULL;
    }
    memset(m_CodeTable, 0, sizeof(m_CodeTable));
}

void CGreyBitCreaterDlg::InitCharset(void)
{
    memset(m_FileIndex, 0, sizeof(m_FileIndex));
	InitCharsetList();
}

void CGreyBitCreaterDlg::InitOutput(void)
{
	InitSizeCombox();
    OnBnClickedRadioVect();
    OnBnClickedRadioBitmap();
}

void CGreyBitCreaterDlg::InitPreview(void)
{
	m_PreviewStatic.ModifyStyle(0xF,SS_BITMAP|SS_CENTERIMAGE);
	m_CodeSlider.SetRange(0,MAX_SLIDER_RANGE);
	m_CodeSlider.SetTicFreq(MAX_SLIDER_RANGE/16);
	m_CodeSlider.SetLineSize(1);
	m_CodeSlider.SetPageSize(MAX_SLIDER_RANGE/16);
	m_CodeEdit.SetLimitText(4);
    m_CodeEdit.SetWindowText(_T("21"));
	m_nCodeStart = 0x21;
    m_nCurrViewer = 0;
    m_ScaleEdit.SetLimitText(3);
    m_ScaleEdit.SetWindowText(_T("100"));
	UpdatePreviewCode();
}

void CGreyBitCreaterDlg::InitBuild(void)
{
	m_nPercent = -1;
	m_ProgressBar.SetRange32(0,100);
	UpdatePercent(0);
	UpdateProgressNum(0,0);
	memset(m_wAllCode, 0, sizeof(m_wAllCode));
    memset(m_nCodeHoriOff, 0, sizeof(m_nCodeHoriOff));
	m_nTotalNum = 0;
	m_nCurrNum  = 0;
}

void CGreyBitCreaterDlg::InitCharsetList(void)
{
    int i;
    GB_UINT16 nMin,nMax;
	CString szTemp;
	m_CodeListCtrl.SetExtendedStyle(m_CodeListCtrl.GetExtendedStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT|LVS_EX_FLATSB);

	szTemp.LoadStringW(IDS_COL_CODERANGE);
	m_CodeListCtrl.InsertColumn(0,szTemp,LVCFMT_LEFT,115);          //添加列标题
    szTemp.LoadStringW(IDS_COL_INPUTSOURCE);
	m_CodeListCtrl.InsertColumn(1,szTemp,LVCFMT_LEFT,100);          //添加列标题
    szTemp.LoadStringW(IDS_COL_CODENUM);
	m_CodeListCtrl.InsertColumn(2,szTemp,LVCFMT_CENTER,50);         //添加列标题
	szTemp.LoadStringW(IDS_COL_COMMENT);
	m_CodeListCtrl.InsertColumn(3,szTemp,LVCFMT_LEFT,500);
    m_CodeListCtrl.DeleteAllItems();
    
	for(i = 0; i < UNICODE_SECTION_NUM; i++)
    {
        UnicodeSection_GetSectionInfo(i, &nMin, &nMax);
        szTemp.Format(_T("0x%04X-0x%04X"),nMin,nMax);
        m_CodeListCtrl.InsertItem(i,szTemp);
        GetFileName(m_InputFile[m_FileIndex[i]], szTemp);
        //szTemp.Format(_T("%s"),m_FileIndex[i]);
        m_CodeListCtrl.SetItemText(i,1,szTemp);
        szTemp.Format(_T("%d"),m_CodeTable[0][i]);
        m_CodeListCtrl.SetItemText(i,2,szTemp);
        szTemp.LoadStringW(i+IDS_0000_007F);
		m_CodeListCtrl.SetItemText(i,3,szTemp);
    }
    m_SelectAllCheck.SetCheck(false);
}

void CGreyBitCreaterDlg::InitSizeCombox(void)
{
	CString szSize;
	int nSel = 0;
	for(int i=60;i>7;i=i-2)
	{
		szSize.Format(_T("%d"),i);
		m_SizeComboBox.InsertString(0, szSize);
		if(i < 20)
		{
			nSel++;
		}
	}
	m_SizeComboBox.SetCurSel(nSel);
}

// Get Parameter
int CGreyBitCreaterDlg::GetFontSize(void)
{
	CString szSize;
	
	// 处理文本字符
	m_SizeComboBox.GetWindowText(szSize);
	return _tstoi(szSize);
}

int CGreyBitCreaterDlg::GetFontScale(void)
{
	CString szScale;
	
    m_ScaleEdit.GetWindowText(szScale);
	return _tstoi(szScale);
}

int CGreyBitCreaterDlg::GetIndexByName(CString &szName)
{
    CString szTemp;
    for(int i=0; i<m_InputFileNum; i++)
    {
        GetFileName(m_InputFile[i], szTemp);
        if(szName == szTemp)
        {
            return i;
        }
    }
    return 0;
}

void CGreyBitCreaterDlg::GetFileName(CString &szPath, CString &szName)
{
    TCHAR *pChar = szPath.GetBuffer();
    for(int i=szPath.GetLength()-1; i>=0; i--)
    {
        if(pChar[i] == _T('\\') || pChar[i] == _T('/'))
        {
            pChar += i+1;
            break;
        }
    }
    
    szName.SetString(pChar);
}

CString &CGreyBitCreaterDlg::GetOutputName(void)
{
	m_OutputPathCombo.GetWindowText(m_OutputName);
	return m_OutputName;
}

// Update
void CGreyBitCreaterDlg::UpdateCharsetList(void)
{
    int i;
    CString szTemp;
    
    if(m_InputFileNum > 0)
    {
        for(i = 0; i < UNICODE_SECTION_NUM; i++)
        {
            szTemp.Format(_T("%d"),m_CodeTable[m_FileIndex[i]][i]);
            m_CodeListCtrl.SetItemText(i,2,szTemp);
            if(m_CodeTable[m_FileIndex[i]][i]>0)
            {
                m_CodeListCtrl.SetCheck(i, TRUE);
                GetFileName(m_InputFile[m_FileIndex[i]], szTemp);
                m_CodeListCtrl.SetItemText(i,1,szTemp);
            }
            else
            {
                m_CodeListCtrl.SetCheck(i, FALSE);
                szTemp.SetString(_T(""));
                m_CodeListCtrl.SetItemText(i,1,szTemp);
            }
        }
    }
}

void CGreyBitCreaterDlg::UpdatePercent(int nPercent)
{
	if(m_nPercent != nPercent)
	{
		CString szTemp;
		szTemp.Format(_T("%d"),nPercent);
		szTemp += _T("%");
		m_PercentStatic.SetWindowText(szTemp);
		m_ProgressBar.SetPos(nPercent);
		m_nPercent = nPercent;
	}
}

void CGreyBitCreaterDlg::UpdateProgressNum(int nCurr, int nTotal)
{
	CString szTemp;
	szTemp.Format(_T("%d/%d"),nCurr, nTotal);
	m_TotalStatic.SetWindowText(szTemp);
}

void CGreyBitCreaterDlg::UpdateBuildInfo(void)
{
	m_nCurrNum = m_GreyBitBuild.GetBuildNum();
	if(m_nTotalNum > 0)
	{
		UpdatePercent((m_nCurrNum*100)/m_nTotalNum);
	}
	UpdateProgressNum(m_nCurrNum, m_nTotalNum);
}

void CGreyBitCreaterDlg::UpdatePreviewCode(void)
{
	CString szCode, szItem, szScale;
	int nListIdx;
        
	m_CodeEdit.GetWindowText(szCode);
	m_nPreviewCode = _tcstol(szCode, NULL, 16);

    nListIdx = UnicodeSection_GetIndex(m_nPreviewCode);
	szItem = m_CodeListCtrl.GetItemText(nListIdx,1);
    m_nCurrViewer = GetIndexByName(szItem);
    
    if(m_pViewer[m_nCurrViewer])
    {
	    m_pViewer[m_nCurrViewer]->SetFontSize(GetFontSize(), GetFontScale());
    }
	UpdateCodeBitmap(m_nPreviewCode, m_nCurrViewer);
}

void CGreyBitCreaterDlg::UpdateCodeBitmap(int nCode, int nIndex)
{
	BITMAP hbitmap;
	CBitmap myCodeBmp;
    if(m_pViewer[nIndex])
    {
        m_pViewer[nIndex]->LoadGlyph((int)nCode, m_OutlineCheck.GetCheck());
        myCodeBmp.CreateCompatibleBitmap(m_PreviewStatic.GetDC(),m_pViewer[nIndex]->GetWidth(),m_pViewer[nIndex]->GetHeight());
        
	    if(myCodeBmp.GetBitmap(&hbitmap))
	    {
		    int nSrcPitch = m_pViewer[nIndex]->GetPitch();
		    int nDstPitch = hbitmap.bmWidthBytes;
		    int RptLen    = hbitmap.bmBitsPixel>>3;
		    int DataLen   = hbitmap.bmHeight*hbitmap.bmWidthBytes*RptLen;

		    if(RptLen == 4)
		    {
			    byte *pData = new byte[DataLen];
			    byte *pSrc  = m_pViewer[nIndex]->GetBits();
			    byte *pDst  = pData;
			    for(int y=0;y<hbitmap.bmHeight;y++)
			    {
				    for(int x=0;x<hbitmap.bmWidth;x++)
				    {
					    pDst[x*RptLen]   = 255-pSrc[x];
					    pDst[x*RptLen+1] = 255-pSrc[x];
					    pDst[x*RptLen+2] = 255-pSrc[x];
					    pDst[x*RptLen+3] = 255;
				    }
				    pSrc += nSrcPitch;
				    pDst += nDstPitch;
			    }
			    myCodeBmp.SetBitmapBits(DataLen, pData);
			    delete pData;

			    UpdateCodeFontInfo(hbitmap.bmWidth, hbitmap.bmHeight,m_pViewer[nIndex]->GetHoriOff());
			    m_PreviewStatic.SetBitmap(myCodeBmp);
		    }
	    }
    }
}

void CGreyBitCreaterDlg::UpdateCodeFontInfo(int nWidth, int nHeight, int nHoriOff)
{
	CString szTemp;
	szTemp.Format(_T("%d"),nWidth);
	m_WidthStatic.SetWindowText(szTemp);
	szTemp.Format(_T("%d"),nHeight);
	m_HeightStatic.SetWindowText(szTemp);
    szTemp.Format(_T("%d"),nHoriOff);
	m_HOffStatic.SetWindowText(szTemp);
}

void CGreyBitCreaterDlg::UpdateFontViewer(void)
{
    int i;
    for(i=0; i<= CODE_ST_IDX; i++)
    {
        if(m_pViewer[i])
        {
            m_GreyBitFactory.DestroyViewer(m_pViewer[i]);
            m_pViewer[i] = 0;
        }
    }
    memset(m_FileIndex, 0, sizeof(m_FileIndex));
    for( i=0; i< m_InputFileNum; i++)
    {
        m_pViewer[i] = m_GreyBitFactory.CreateViewer(m_InputFile[i].GetBuffer());
        if(m_pViewer[i])
        {
            for(int j=0; j<UNICODE_SECTION_NUM;j++)
            {
                GB_UINT16 nMin,nMax;
                m_CodeTable[i][j] = 0;
                UnicodeSection_GetSectionInfo(j, &nMin, &nMax);
                for(int nCode = nMin; nCode<= nMax; nCode++)
                {
                    if(m_pViewer[i]->IsCodeAvaliable(nCode))
                    {
                        m_CodeTable[i][j]++;
                    }
                }

                if(m_FileIndex[j] == 0 && m_CodeTable[i][j] > 0)
                {
                    m_FileIndex[j] = i;
                }
            }
        }
    }
}

// Parameter Check
bool CGreyBitCreaterDlg::IsSizeAvaliable(void)
{
	CString szSize;
	
	// 处理文本字符
	m_SizeComboBox.GetWindowText(szSize);

	if(szSize.IsEmpty())
	{
		return false;
	}

	if(szSize.GetLength()>3)
	{
		return false;
	}

	for(int i=0; i<szSize.GetLength(); i++)
	{
		TCHAR c = szSize.GetAt(i);
		if(c>_T('9') || c<_T('0'))
		{
			return false;
		}
	}
	return true;
}

bool CGreyBitCreaterDlg::IsCodeAvaliable(void)
{
	CString szCode;
	
	// 处理文本字符
	m_CodeEdit.GetWindowText(szCode);

	if(szCode.IsEmpty())
	{
		return false;
	}

	if(szCode.GetLength()>4)
	{
		return false;
	}
	
	for(int i=0; i<szCode.GetLength(); i++)
	{
		TCHAR c = szCode.GetAt(i);
		if((c>_T('9') || c<_T('0'))&&(c>_T('F') || c<_T('A')))
		{
			return false;
		}
	}
	return true;
}

bool CGreyBitCreaterDlg::IsCodeAvaliableInSrc(int nCode, int nIndex, int *pIdxOut)
{
    int i;
    if(nIndex < 0)
    {
        for( i=0; i< m_InputFileNum; i++)
        {
            if(m_pViewer[i])
            {
                if(m_pViewer[i]->IsCodeAvaliable(nCode))
                {
                    if(pIdxOut)
                    {
                        *pIdxOut = i;
                    }
                    return true;
                }
            }
        }
    }
    else
    {
        if(m_pViewer[nIndex])
        {
            if(m_pViewer[nIndex]->IsCodeAvaliable(nCode))
            {
                if(pIdxOut)
                {
                    *pIdxOut = nIndex;
                }
                return true;
            }
        }
    }
	return false;
}

// Input Parse
int CGreyBitCreaterDlg::ParseAllCode(void)
{
	int nNum = 0;
	InitBuild();
    nNum += ParseCharsetList();
    nNum += ParseBmpCode();
    nNum += ParseTxtCode();
	return nNum;
}

int CGreyBitCreaterDlg::ParseCharsetList(void)
{
	int nNum = 0;
	GB_UINT16 nMin,nMax;
	int nCodeSrcIdx;
	CString szItem;
    
	for(int i = 0;i < m_CodeListCtrl.GetItemCount();i++)
	{
		if(m_CodeListCtrl.GetCheck(i))
		{
			szItem = m_CodeListCtrl.GetItemText(i,1);
			nCodeSrcIdx = GetIndexByName(szItem);
            UnicodeSection_GetSectionInfo(i, &nMin, &nMax);
			for(int j=nMin; j<=nMax; j++)
			{
				if(IsCodeAvaliableInSrc(j, nCodeSrcIdx))
				{
					if(m_wAllCode[j] == 0)
					{
						nNum++;
                        m_wAllCode[j] |= (nCodeSrcIdx&CODE_ST_IDX)|CODE_ST_ON;
					}
				}
			}
		}
	}
	return nNum;
}

int CGreyBitCreaterDlg::ParseBmpCode(void)
{
	int nNum = 0;

    if(!m_BmpCheck.GetCheck() || m_VectorRadio.GetCheck())
	{
		return nNum;
	}

	while(1)
	{
		CFileFind myFileFind;
		CString   szName;
        CString   szHori;
        CString   szFind;
		BOOL      bFind;
		int       nCode;
        CFile     myFile;

		m_InputBmpCombo.GetWindowText(m_BmpPath);
		if(m_BmpPath.IsEmpty())
		{
			break;
		}
		
		szFind.Format(_T("%s\\*.bmp"), m_BmpPath);
		bFind = myFileFind.FindFile(szFind);
	    while(bFind)
	    {
            bFind = myFileFind.FindNextFile();
		    szName = myFileFind.GetFileName();
		    szName.Replace(_T('.'),_T('\0'));
		    nCode  = _tcstol(szName, NULL, 16);
		    if(m_wAllCode[nCode] == 0)
		    {
			    nNum++;
		    }
		    m_wAllCode[nCode] |= CODE_ST_BMP|CODE_ST_ON;

            // 获取字符水平偏移
            szHori = myFileFind.GetFilePath();
            szHori.Replace(_T(".bmp"),_T(".hori"));
            
		    if (myFile.Open(szHori, CFile::modeRead))
		    {
                int   nFileLen;
                char *pTxtBuffer;
                
			    // 读取数据到内存
		        nFileLen = (int)myFile.GetLength();
                if(nFileLen > 0)
                {
		            pTxtBuffer = new char[nFileLen+1];
		            myFile.Read(pTxtBuffer, nFileLen);
                    m_nCodeHoriOff[nCode] = atoi(pTxtBuffer);
                    delete pTxtBuffer;
                }
                else
                {
                    // 水平偏移为负的宽度
                    m_nCodeHoriOff[nCode] = MAX_CHAR_CODE;
                }
		        myFile.Close();
		    }
	    }
		break;
	}
	return nNum;
}

int CGreyBitCreaterDlg::ParseTxtCode(void)
{
	int nNum = 0;
	byte *pTxtBuffer;
	byte *pCurrPtr;
	int nTxtTotalChar = 0;

	if(m_TxtCheck.GetCheck() == false)
	{
		return nNum;
	}
	
	while(1)
	{
		CString sFileName;

		// 处理文本字符
		m_InputTxtCombo.GetWindowText(sFileName);

		if(true == sFileName.IsEmpty())
		{
			break;
		}
		
		// 打开文件
		CFile myFile;
		int   nFileLen;

		if ( false == myFile.Open(sFileName, CFile::modeRead))
		{
			break;
		}
		
		// 读取数据到内存
		nFileLen = (int)myFile.GetLength();
		pTxtBuffer = new byte[nFileLen];
		myFile.Read(pTxtBuffer, nFileLen);
		myFile.Close();
		
		if(nFileLen == 0)
		{
			break;
		}

		// 判断数据流的头，获取转换的基本信息
		if(pTxtBuffer[0] == 0xFF 
			&& pTxtBuffer[1] == 0xFE)
		{
			// Little Endian string
			pCurrPtr = &pTxtBuffer[2];
			nTxtTotalChar = (nFileLen-2)/2;
		}
		else if(pTxtBuffer[0] == 0xFE 
			&& pTxtBuffer[1] == 0xFF)
		{
			// Big Endian string
			int i,idx;
			byte temp;

			pCurrPtr = &pTxtBuffer[2];
			nTxtTotalChar = (nFileLen-2)/2;
            
			// 转换成Litttle Endian形式
			for(i=0; i<nTxtTotalChar; i++)
			{
				idx = i*2;
				temp = pCurrPtr[idx];
				pCurrPtr[idx] = pCurrPtr[idx+1];
				pCurrPtr[idx+1] = temp;
			}
		}
		else
		{
			// DBCS string
			wchar_t *pTempW;

			//System.Text.UnicodeEncoding myUnicode;
			nTxtTotalChar = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)pTxtBuffer, nFileLen, NULL, 0);
			
			pTempW = new wchar_t[nTxtTotalChar];
			nTxtTotalChar = MultiByteToWideChar(CP_ACP, 0, (LPCSTR)pTxtBuffer, nFileLen, pTempW, nTxtTotalChar);
			delete pTxtBuffer;
			pTxtBuffer = (byte *)pTempW;
			pCurrPtr = &pTxtBuffer[0];
		}
		break;
	}
	
	wchar_t *pCode = (wchar_t *)pCurrPtr;
	int nCode;
	int nCodeSrcIdx;
    byte bTempCode[MAX_CHAR_CODE+1];
    memset(bTempCode, 0, sizeof(bTempCode));
	for(int i=0;i<nTxtTotalChar;i++)
	{
		nCode = (int)pCode[i];
        bTempCode[nCode] = CODE_ST_ON;

		if(IsCodeAvaliableInSrc(nCode, -1, &nCodeSrcIdx))
		{
			if(m_wAllCode[nCode] == 0)
			{
				nNum++;
                m_wAllCode[nCode] |= (nCodeSrcIdx&CODE_ST_IDX)|CODE_ST_ON;
			}
		}
	}

    for(int i=0;i<=MAX_CHAR_CODE;i++)
	{
		if(m_wAllCode[i] & CODE_ST_BMP)
		{
			if(!bTempCode[i])
            {
                m_wAllCode[i] = m_wAllCode[i]&(~CODE_ST_BMP);
                nNum--;
            }
		}
	}

	// 释放资源
	if(NULL != pTxtBuffer)
	{
		delete pTxtBuffer;
	}
	return nNum;
}

// Build
void CGreyBitCreaterDlg::StartBuild(void)
{
    CString szTemp;
    int Idx;
    
	m_GreyBitBuild.SetBmpPath(m_BmpPath);
	m_GreyBitBuild.SetCodeTable(m_wAllCode,m_nTotalNum);
    m_GreyBitBuild.SetCodeHoriOff(m_nCodeHoriOff);
	m_GreyBitBuild.SetCompress(m_CompressRadio.GetCheck()!=0);
	m_GreyBitBuild.SetGrey(m_GreyRadio.GetCheck()!=0);
    m_GreyBitBuild.SetVector(m_VectorRadio.GetCheck()!=0);
	m_GreyBitBuild.SetSize(GetFontSize());
    m_GreyBitBuild.SetScale(GetFontScale());
    for(int i=0; i<m_InputFileNum; i++)
    {
	    m_GreyBitBuild.SetInputFile(m_InputFile[i], i);
    }
    
	if(!m_GreyBitBuild.CheckInputFileAvaliable(&Idx) && m_VectorRadio.GetCheck())
	{
		CString szTitle;
		CString szContent;

		szTitle.LoadString(IDS_TITLE_ERROR);
		if(Idx < 0)
		{
			szContent.LoadString(IDS_MSG_NOINPUTFILE);
		}
		else
		{
			szContent.Format(IDS_MSG_FILENOTOPEN,m_InputFile[Idx]);
		}
		MessageBox(szContent,szTitle,MB_OK);
		return;
	}

    szTemp = GetOutputName();
    if(szTemp.IsEmpty())
    {
        CString szTitle;
		CString szContent;

		szTitle.LoadString(IDS_TITLE_ERROR);
	    szContent.LoadString(IDS_MSG_NOOUTPUTFILE);
		MessageBox(szContent,szTitle,MB_OK);
		return;
    }
    
	m_GreyBitBuild.SetOutputName(szTemp);
	m_GreyBitBuild.SetCWnd(this);
	m_GreyBitBuild.Start();

    szTemp.LoadStringW(IDS_BUILD_STOP);
	m_BuildButton.SetWindowText(szTemp);
}

void CGreyBitCreaterDlg::StopBuild(void)
{
	m_GreyBitBuild.Stop();

	CString szTemp;
	szTemp.LoadStringW(IDS_BUILD_START);
	m_BuildButton.SetWindowText(szTemp);
}

// Message Proccess

LRESULT CGreyBitCreaterDlg::OnMyUpdateInfo(WPARAM wParam, LPARAM lParam)
{
	UpdateBuildInfo();
	if(!IsBuilding())
	{
		StopBuild();
	}
	return 0;
}

LRESULT CGreyBitCreaterDlg::OnMyShowCtrl(WPARAM wParam, LPARAM lParam)
{
    int nItem = wParam;
    int nSubItem = lParam;
    
    if(nSubItem == 1 && m_InputFileNum > 0)
    {
        CString szName;
        m_ArraySrc.RemoveAll();
        for(int i=0; i<m_InputFileNum; i++)
        {
            if(m_CodeTable[i][nItem] > 0)
            {
                GetFileName(m_InputFile[i],szName);
                m_ArraySrc.Add(szName);
            }
        }
        m_CodeListCtrl.UpdateColumn(1, COLUMN_STYLE_COMBOBOX, &m_ArraySrc);
    }
    return 0;
}

LRESULT CGreyBitCreaterDlg::OnItemChanged(WPARAM wParam, LPARAM lParam)
{
    int nItem = wParam;
    int nSubItem = lParam;
    
    if(nSubItem == 1 && m_InputFileNum > 0)
    {
        CString szTemp;
        
        m_FileIndex[nItem] = GetIndexByName(m_CodeListCtrl.GetItemText(nItem, nSubItem));
        szTemp.Format(_T("%d"),m_CodeTable[m_FileIndex[nItem]][nItem]);
        m_CodeListCtrl.SetItemText(nItem,2,szTemp);
    }
    return 0;
}

void CGreyBitCreaterDlg::OnBnClickedButtonInputOpen()
{
	CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    szFilterTemp.LoadStringW(IDS_FILTER_VECTOR);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_VECTOR_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");
	szFilterTemp.LoadStringW(IDS_FILTER_BITMAP);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_BITMAP_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_ALL);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_ALL_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");

    //no more so append final pipe
	szFilter += _T("|");

    TCHAR sBuffer[6000];
    sBuffer[0] = 0; 
	//myFileDlg.m_ofn.lpstrFilter = szFilter;
    CFileDialog myFileDlg( true, 
		                   NULL, 
						   NULL, 
						   OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_NOCHANGEDIR,
						   szFilter,
						   NULL);
    myFileDlg.m_ofn.lpstrFile   =   sBuffer;   
    myFileDlg.m_ofn.nMaxFile    =   6000; 
    if(myFileDlg.DoModal() == IDOK)
    {
		int nIndex;
		
        //复制文件名
        POSITION mPos = myFileDlg.GetStartPosition();
        m_InputFileNum = 0;
        
        while(mPos!=NULL)
        {
            if(m_InputFileNum > 0)
            {
                szFileName += _T(";");
            }
            m_InputFile[m_InputFileNum] = myFileDlg.GetNextPathName(mPos);
            szFileName += m_InputFile[m_InputFileNum];
            m_InputFileNum++;
            if(m_InputFileNum >= CODE_ST_IDX)
            {
                break;
            }
        }
        
	    nIndex = m_InputFileCombo.SelectString(0, szFileName);
        if(nIndex == CB_ERR)
        {
            m_InputFileCombo.InsertString(0, szFileName);
            m_InputFileCombo.SetCurSel(0);
        }
        UpdateFontViewer();
		UpdateCharsetList();
    }
}

void CGreyBitCreaterDlg::OnBnClickedButtonInputTxt()
{
	CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    szFilterTemp.LoadStringW(IDS_FILTER_TXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_TXT_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_ALL);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_ALL_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");

    //no more so append final pipe
	szFilter += _T("|");

	//myFileDlg.m_ofn.lpstrFilter = szFilter;
    CFileDialog myFileDlg( true, 
		                   NULL, 
						   NULL, 
						   OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR,
						   szFilter,
						   NULL);
    if(myFileDlg.DoModal() == IDOK)
    {
		int nIndex;
		
        //复制文件名
        szFileName = myFileDlg.GetPathName();
		
		nIndex = m_InputTxtCombo.SelectString(0, szFileName);
        if(nIndex == CB_ERR)
        {
            m_InputTxtCombo.InsertString(0, szFileName);
            m_InputTxtCombo.SetCurSel(0);
        }
    }
}

void CGreyBitCreaterDlg::OnBnClickedButtonInputBmp()
{
	CString myPath;//当前打开目录路径
	BROWSEINFO  bf;
	LPITEMIDLIST pidlbf;
	CString     szFilterTemp;

	ZeroMemory((LPVOID)&bf,sizeof(BROWSEINFO));
	bf.hwndOwner=this->m_hWnd;
	bf.pszDisplayName = myPath.GetBuffer(MAX_PATH);
	szFilterTemp.LoadString(IDS_TITLE_BMPPATH);
	bf.lpszTitle = szFilterTemp;
	bf.ulFlags=BIF_RETURNFSANCESTORS | BIF_RETURNONLYFSDIRS;
	bf.lpfn=NULL;
	bf.lParam=0;
	if((pidlbf=::SHBrowseForFolder(&bf))==NULL) 
	{
		return;
	}

	WCHAR temp[MAX_PATH];
	if(::SHGetPathFromIDList(pidlbf,temp)==NULL)
	{
		GlobalFree(pidlbf);
		return;
	}
	GlobalFree(pidlbf);
	myPath.Format(temp);
	
    m_InputBmpCombo.InsertString(0, myPath);
    m_InputBmpCombo.SetCurSel(0);
}

void CGreyBitCreaterDlg::OnBnClickedButtonOutputOpen()
{
    CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    if(m_VectorRadio.GetCheck())
    {
        szFilterTemp.LoadStringW(IDS_FILTER_GVF);
	    szFilter += szFilterTemp;
        szFilter += _T("|");
        szFilterTemp.LoadStringW(IDS_FILTER_GVF_EXT);
	    szFilter += szFilterTemp;
        szFilter += _T("|");
    }
    else
    {
        szFilterTemp.LoadStringW(IDS_FILTER_GBF);
	    szFilter += szFilterTemp;
        szFilter += _T("|");
        szFilterTemp.LoadStringW(IDS_FILTER_GBF_EXT);
	    szFilter += szFilterTemp;
        szFilter += _T("|");
    }

    //no more so append final pipe
	szFilter += _T("|");

	//myFileDlg.m_ofn.lpstrFilter = szFilter;
    CFileDialog myFileDlg( true, 
		                   NULL, 
						   NULL, 
						   OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
						   szFilter,
						   NULL);
    if(myFileDlg.DoModal() == IDOK)
    {
		int nIndex;
		
        //复制文件名
        szFileName = myFileDlg.GetPathName();
        
        bool bFind = false;
        // 检查扩展名是否正确
        for(int i=szFileName.GetLength()-1;i>=0;i--)
        {
            if(szFileName[i] == _T('.'))
            {
                bFind = true;
                break;
            }
        }

        if(!bFind)
        {
            if(m_VectorRadio.GetCheck())
            {
                szFileName.Append(_T(".gvf"));
            }
            else
            {
                szFileName.Append(_T(".gbf"));
            }
        }
		
		nIndex = m_OutputPathCombo.SelectString(0, szFileName);
        if(nIndex == CB_ERR)
        {
            m_OutputPathCombo.InsertString(0, szFileName);
            m_OutputPathCombo.SetCurSel(0);
        }
    }
}

void CGreyBitCreaterDlg::OnBnClickedBuild()
{
	if(IsBuilding())
	{
		StopBuild();
	}
	else
	{
		m_nTotalNum = ParseAllCode();
		UpdateBuildInfo();
		if(m_nTotalNum == 0)
		{
			CString szTitle;
			CString szContent;

			szTitle.LoadString(IDS_TITLE_ERROR);
			szContent.LoadString(IDS_MSG_NOTHING);
			MessageBox(szContent,szTitle,MB_OK);
			return;
		}
		StartBuild();
	}
}

void CGreyBitCreaterDlg::OnBnClickedButtonSave()
{
    CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    szFilterTemp.LoadStringW(IDS_FILTER_BMP);
    szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_BMP_EXT);
    szFilter += szFilterTemp;
    szFilter += _T("|");

    //no more so append final pipe
	szFilter += _T("|");

    if(m_pViewer[m_nCurrViewer])
    {
        szFileName.Format(_T("0x%04X.bmp"), m_nPreviewCode);
    }
    else
    {
        CString szTitle;
		CString szContent;

		szTitle.LoadString(IDS_TITLE_WARNING);
		szContent.LoadString(IDS_MSG_NOIMAGE);
		MessageBox(szContent,szTitle,MB_OK);
        return;
    }
	//myFileDlg.m_ofn.lpstrFilter = szFilter;
    CFileDialog myFileDlg( false, 
		                   NULL, 
						   szFileName, 
						   OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
						   szFilter,
						   NULL);
    if(myFileDlg.DoModal() == IDOK)
    {
        CDib myDIB;
        //复制文件名
        szFileName = myFileDlg.GetPathName();
        
        bool bFind = false;
        // 检查扩展名是否正确
        for(int i=szFileName.GetLength()-1;i>=0;i--)
        {
            if(szFileName[i] == _T('.'))
            {
                bFind = true;
                break;
            }
        }

        if(!bFind)
        {
            szFileName.Append(_T(".bmp"));
        }
        
        if(m_pViewer[m_nCurrViewer])
        {
            myDIB.Save( myFileDlg.GetPathName(), 
                        m_pViewer[m_nCurrViewer]->GetWidth(), 
                        -m_pViewer[m_nCurrViewer]->GetHeight(), 
                        m_pViewer[m_nCurrViewer]->GetPitch(),
                        8, 
                        (LPSTR)m_pViewer[m_nCurrViewer]->GetBits());
        }
    }
}

void CGreyBitCreaterDlg::OnBnClickedCheckCharsetAll()
{
	int bSelectAll = m_SelectAllCheck.GetCheck();
	for(int i=0; i<m_CodeListCtrl.GetItemCount();i++)
	{
		m_CodeListCtrl.SetCheck(i,bSelectAll);
	}
}

void CGreyBitCreaterDlg::OnCbnEditchangeComboSize()
{
	if(!IsSizeAvaliable())
	{
		CString szSize;
		
		m_SizeComboBox.GetWindowText(szSize);
		if(szSize.GetLength()>0)
		{
			szSize.Delete(szSize.GetLength()-1);
			m_SizeComboBox.SetWindowText(szSize);
			m_SizeComboBox.SetEditSel(szSize.GetLength(),szSize.GetLength());
		}
	}
}

void CGreyBitCreaterDlg::OnEnChangeEditCode()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	if(!IsCodeAvaliable())
	{
		CString szCode;
		
		m_CodeEdit.GetWindowText(szCode);

		if(szCode.GetLength()>0)
		{
			szCode.Delete(szCode.GetLength()-1);
			m_CodeEdit.SetWindowText(szCode);
			m_CodeEdit.SetSel(szCode.GetLength(),szCode.GetLength());
		}
	}
}

void CGreyBitCreaterDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CWnd *pSlider = GetDlgItem(IDC_SLIDER_CODE); 
	if(pSlider == pScrollBar)
	{
		CString szCode;
		int nCode = m_nCodeStart+m_CodeSlider.GetPos();
		if(nCode <= MAX_CHAR_CODE)
		{
			szCode.Format(_T("%X"),nCode);
			m_CodeEdit.SetWindowText(szCode);
			UpdatePreviewCode();
		}
	}

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CGreyBitCreaterDlg::OnBnClickedButtonGoto()
{
	UpdatePreviewCode();
	m_nCodeStart = m_nPreviewCode;
	m_CodeSlider.SetPos(0);
}

void CGreyBitCreaterDlg::OnCbnCloseupComboInput()
{
    if(m_InputComboSel != m_InputFileCombo.GetCurSel())
    {
        // 分解文件名
        CString szFileNames;
        TCHAR *pStr;
        TCHAR *pStrEnd;
        

        m_InputFileCombo.GetWindowTextW(szFileNames);
        szFileNames.Replace(_T(';'),_T('\0'));
        pStr = szFileNames.GetBuffer();
        pStrEnd = pStr+szFileNames.GetLength();
        m_InputFileNum = 0;
        while(pStr < pStrEnd)
        {
            m_InputFile[m_InputFileNum].SetString(pStr);
            pStr += m_InputFile[m_InputFileNum].GetLength()+1;
            m_InputFileNum++;
            if(m_InputFileNum >= CODE_ST_IDX)
            {
                break;
            }
        }
        UpdateFontViewer();
        UpdateCharsetList();
    }
}

void CGreyBitCreaterDlg::OnCbnDropdownComboInput()
{
    m_InputComboSel = m_InputFileCombo.GetCurSel();
}

void CGreyBitCreaterDlg::OnBnClickedRadioVect()
{
    if(m_VectorRadio.GetCheck())
    {
        CButton* pButton;
        m_GreyRadio.EnableWindow(FALSE);
        pButton = (CButton*)GetDlgItem(IDC_RADIO_MONO);
        pButton->EnableWindow(FALSE);
        m_CompressRadio.EnableWindow(FALSE);
        pButton = (CButton*)GetDlgItem(IDC_RADIO_UNCOMPRESS);
        pButton->EnableWindow(FALSE);
        m_InputBmpCombo.EnableWindow(FALSE);
        m_BmpCheck.EnableWindow(FALSE);

        CString     szFileName;
        m_OutputPathCombo.GetWindowText(szFileName);
        szFileName.Replace(_T(".gbf"),_T(".gvf"));
        m_OutputPathCombo.SetWindowText(szFileName);
    }
}

void CGreyBitCreaterDlg::OnBnClickedCheckOutline()
{
    UpdatePreviewCode();
}

void CGreyBitCreaterDlg::OnBnClickedRadioBitmap()
{
    if(!m_VectorRadio.GetCheck())
    {
        CButton* pButton;
        m_GreyRadio.EnableWindow();
        pButton = (CButton*)GetDlgItem(IDC_RADIO_MONO);
        pButton->EnableWindow();
        if(m_GreyRadio.GetCheck())
        {
            m_CompressRadio.EnableWindow();
            pButton = (CButton*)GetDlgItem(IDC_RADIO_UNCOMPRESS);
            pButton->EnableWindow();
        }
        else
        {
            m_CompressRadio.EnableWindow(FALSE);
            pButton = (CButton*)GetDlgItem(IDC_RADIO_UNCOMPRESS);
            pButton->EnableWindow(FALSE);
        }
        m_InputBmpCombo.EnableWindow();
        m_BmpCheck.EnableWindow();

        CString     szFileName;
        m_OutputPathCombo.GetWindowText(szFileName);
        szFileName.Replace(_T(".gvf"),_T(".gbf"));
        m_OutputPathCombo.SetWindowText(szFileName);
    }
}

//void CGreyBitCreaterDlg::OnLvnItemchangedListCharset(NMHDR *pNMHDR, LRESULT *pResult)
//{
//    LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
//    // TODO: 在此添加控件通知处理程序代码
//    *pResult = 0;
//}

void CGreyBitCreaterDlg::OnBnClickedRadioGrey()
{
    if(m_GreyRadio.GetCheck())
    {
        CButton* pButton;
        m_CompressRadio.EnableWindow();
        pButton = (CButton*)GetDlgItem(IDC_RADIO_UNCOMPRESS);
        pButton->EnableWindow();
    }
}

void CGreyBitCreaterDlg::OnBnClickedRadioMono()
{
    if(!m_GreyRadio.GetCheck())
    {
        CButton* pButton;
        m_CompressRadio.EnableWindow(FALSE);
        pButton = (CButton*)GetDlgItem(IDC_RADIO_UNCOMPRESS);
        pButton->EnableWindow(FALSE);
    }
}

void CGreyBitCreaterDlg::OnBnClickedButtonGcf()
{
    CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    if(m_InputFileNum < 1)
    {
        CString szTitle;
		CString szContent;

		szTitle.LoadString(IDS_TITLE_WARNING);
		szContent.LoadString(IDS_MSG_NOINPUTFILE);
		MessageBox(szContent,szTitle,MB_OK);
        return;
    }

    szFilterTemp.LoadStringW(IDS_FILTER_GCF);
    szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_GCF_EXT);
    szFilter += szFilterTemp;
    szFilter += _T("|");

    //no more so append final pipe
	szFilter += _T("|");

	//myFileDlg.m_ofn.lpstrFilter = szFilter;
    CFileDialog myFileDlg( false, 
		                   NULL, 
						   szFileName, 
						   OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
						   szFilter,
						   NULL);
    if(myFileDlg.DoModal() == IDOK)
    {
        CDib myDIB;
        //复制文件名
        szFileName = myFileDlg.GetPathName();
        
        bool bFind = false;
        // 检查扩展名是否正确
        for(int i=szFileName.GetLength()-1;i>=0;i--)
        {
            if(szFileName[i] == _T('.'))
            {
                bFind = true;
                break;
            }
        }

        if(!bFind)
        {
            szFileName.Append(_T(".gcf"));
        }
        
        GreyBitFontEngine  cGreyBitType;
        cGreyBitType.SetFile(szFileName.GetBuffer());
        for(int i=0; i<m_InputFileNum; i++)
        {
            cGreyBitType.CombineFile(m_InputFile[i].GetBuffer());
        }
        cGreyBitType.FinishCreate();

    }
}
