#pragma once
#include "windows.h"
#include "GreyBitType.h"
#include "GreyBitViewer.h"

class GreyBitFontEngine : public GreyBitEditor
{
private:
	GBHANDLE    m_Library;   /* handle to library     */
    GBHANDLE    m_Creator;      /* handle to face object */
	TCHAR *     m_pFileNamePtr;
    GB_Bitmap   m_Bitmap;
    int         m_Index;
	
public:
	GreyBitFontEngine(void);
	~GreyBitFontEngine(void);

public:
	bool SetFile(TCHAR *pName);
    int PrepareCreate(int nHeight, int nBitCount, bool bCompress);
    int SetBits(int nCode, byte *pBuff, int nWidth, int nBitCount, int nHoriOff);
    int SetOutline(int nCode, byte *pBuff, int nWidth, int nHoriOff);
    int CombineFile(TCHAR *pName);
    int FinishCreate(void);
    void Destroy(void)      {delete this;};
};

//-----------------------------------------------------------------------------
class GreyBitViewFontEngine : public GreyBitViewer
{
private:
	GBHANDLE    m_Library;   /* handle to library     */
	GBHANDLE    m_Loader;      /* handle to face object */
    GBHANDLE    m_Layout;
	TCHAR *     m_pFileNamePtr;
	int         m_nSize;
    int         m_nScale;
	int         m_nWidth;
	int         m_nHeight;
    int         m_nHoriOff;
	int         m_nBitsLen;
	byte       *m_BitsBuf;
	int         m_nBufLen;
    GB_Bitmap   m_Bitmap;
	
public:
	GreyBitViewFontEngine(void);
	~GreyBitViewFontEngine(void);

public:
	void SetFontSize(int nSize, int nScale);
	bool SetFile(TCHAR *pName);
	bool IsCodeAvaliable(int nCode);
	int LoadGlyph(int nCode,char bShowOutline = 0);
	int GetWidth(void)		{return m_nWidth;};
	int GetHeight(void)		{return m_nHeight;};
    int GetHoriOff(void)    {return m_nHoriOff;};
	int GetPitch(void)		{return m_nWidth;};
	int GetBitsLen(void)	{return m_nBitsLen;};
	byte *GetBits(void)	;
    void *GetOutline(void)  {return NULL;};
    void Destroy(void)      {delete this;};
};
