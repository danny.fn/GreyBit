#include "windows.h"
#include "FreeTypeFontEngine.h"
#include "GreyBitRaster.h"
#include "math.h"
#include "ftbbox.h"

#define  ENABLE_SCALE

FreeTypeFontEngine::FreeTypeFontEngine(void)
{
	FT_Init_FreeType( &m_FTlibrary );
	m_FTface   = NULL;
	m_nSize    = 0;
    m_nScale   = 100;
	m_BitsBuf  = NULL;
	m_nBufLen  = 0;
	m_nBitsLen = 0;
	m_nHeight  = 0;
	m_nWidth   = 0;
	m_pFileNamePtr = NULL;
    m_GBlibrary = GreyBitType_Init();
    m_Outline  = NULL;
}

FreeTypeFontEngine::~FreeTypeFontEngine(void)
{
	if(m_BitsBuf)
	{
		delete [] m_BitsBuf;
	}

    if(m_Outline)
    {
        GreyBitType_Outline_Done(m_GBlibrary, m_Outline);
    }

	if(m_FTface)
	{
		FT_Done_Face    ( m_FTface );
	}
	FT_Done_FreeType( m_FTlibrary );
    GreyBitType_Done(m_GBlibrary);
}

bool FreeTypeFontEngine::SetFile(TCHAR *pName)
{
	if(m_pFileNamePtr == pName)
	{
		return true;
	}

	m_pFileNamePtr = pName;
	if(m_FTface)
	{
		FT_Done_Face    ( m_FTface );
        m_FTface = NULL;
	}
	// DBCS string
	char *pTemp;
	int nTxtTotalChar;

	//System.Text.UnicodeEncoding myUnicode;
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, NULL, 0, NULL, NULL);
	
	pTemp = new char[nTxtTotalChar];
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, pTemp, nTxtTotalChar, NULL, NULL);
	FT_New_Face( m_FTlibrary,
                 pTemp,
                 0,
                &m_FTface );

	int nNewSize = m_nSize;
	m_nSize = 0;
	SetFontSize(nNewSize, m_nScale);
	delete pTemp;
    if(m_FTface == NULL)
    {
        m_pFileNamePtr = NULL;
        return false;
    }
    return true;
}

void FreeTypeFontEngine::SetFontSize(int nSize, int nScale)
{
    m_nScale = nScale;
	if(m_nSize != nSize && m_FTface)
	{
		m_nSize = nSize;
		FT_Set_Pixel_Sizes(m_FTface, 0, m_nSize);
        //FT_Set_Char_Size(m_FTface, 0, m_nSize<<6, 0, 0);
		if(m_BitsBuf)
		{
			delete [] m_BitsBuf;
		}
		m_nBufLen = m_nSize*m_nSize*3;
		m_BitsBuf = new byte[m_nBufLen];
	}
}

bool FreeTypeFontEngine::IsCodeAvaliable(int nCode)
{
	int glyph_index;
	glyph_index = FT_Get_Char_Index( m_FTface, nCode );
	return (glyph_index != 0);
}

int FreeTypeFontEngine::GetDisstance(int x1, int y1, int x2, int y2)
{
    int xx = x1-x2;
    int yy = y1-y2;
    if(xx<0)
    {
        xx = -xx;
    }
    if(yy<0)
    {
        yy = -yy;
    }
    return (int)sqrt((double)(xx*xx+yy*yy));
}

int FreeTypeFontEngine::LoadGlyph(int nCode,char bShowOutline)
{
	int glyph_index;
	int error = -1;
	FT_GlyphSlot  slot;

	slot = m_FTface->glyph;

	glyph_index = FT_Get_Char_Index( m_FTface, nCode );
	if(glyph_index == 0)
	{
		return error;
	}
	
	error = FT_Load_Glyph(m_FTface, glyph_index, FT_LOAD_DEFAULT );
	if(error)
	{
		return error;
	}

	if (m_FTface->glyph->format != FT_GLYPH_FORMAT_BITMAP) {
		FT_Render_Glyph( slot, FT_RENDER_MODE_NORMAL);//FT_RENDER_MODE_NORMAL FT_RENDER_MODE_MONO
	}

    m_nHeight  = m_nSize;//slot->bitmap.rows;
    int xStart = 0;
    int yStart = m_FTface->size->metrics.ascender;// ((m_nHeight*m_FTface->ascender+m_FTface->height/2)/m_FTface->height)<<6;

    if(slot->metrics.horiBearingX < 0)
    {
        // 此时是一个符号类的字符，是与前一个字符组合用的字形
        m_nWidth   = slot->metrics.horiAdvance-slot->metrics.horiBearingX;
        xStart     = slot->metrics.horiBearingX;
    }
    else
    {
        m_nWidth   = slot->metrics.horiAdvance;
    }

    int i;
    FT_BBox bbox;
    FT_Outline_Get_BBox(&slot->outline,&bbox);

    yStart = max(yStart, bbox.yMax);
#ifdef ENABLE_SCALE
    int nScaleYOffset = 0;
    int nCurrScale = 100;
    int nBest, bFound = 0;

    bbox.yMax = -(bbox.yMax-yStart);
    bbox.yMin = -(bbox.yMin-yStart);
    if(bbox.yMax < bbox.yMin)
    {
        FT_Pos temp = bbox.yMin;
        bbox.yMin = bbox.yMax;
        bbox.yMax = temp;
    }
    
    nBest = bbox.yMin+(bbox.yMax-bbox.yMin)/2;
    nScaleYOffset = nBest;
    
    // 最佳的Y方向放大是按图形中心位置上下扩散，并且放大之后的图形不会超出范围
    // 但是有些字形是上下不对称的，因此需要调整这个位置以适应图形放大区域
    for(i=0; i<=nBest; i++)
    {
        if(nScaleYOffset+((bbox.yMin-nScaleYOffset)*m_nScale/100)>=0)
        {
            if(nScaleYOffset+((bbox.yMax-nScaleYOffset)*m_nScale/100)<=(m_nHeight<<6))
            {
                bFound = 1;
                break;
            }
        }
        nScaleYOffset--;
    }
    
    if(bFound == 0)
    {
        nScaleYOffset = nBest;
        for(i=nBest; i<=(m_nHeight<<6); i++)
        {
            if(nScaleYOffset+((bbox.yMin-nScaleYOffset)*m_nScale/100)>=0)
            {
                if(nScaleYOffset+((bbox.yMax-nScaleYOffset)*m_nScale/100)<=(m_nHeight<<6))
                {
                    bFound = 1;
                    break;
                }
            }
            nScaleYOffset++;
        }
    }
    
    if(bFound)
    {
        nCurrScale = m_nScale;
    }
#endif

    m_nWidth   = m_nWidth > slot->metrics.width?m_nWidth : slot->metrics.width;
    m_nWidth   = (m_nWidth+63)>>6;
#ifdef ENABLE_SCALE
    m_nWidth   = m_nWidth*nCurrScale/100;
#endif
    //m_nWidth   = m_nWidth+m_nWidth%2;
	m_nBitsLen = m_nHeight*m_nWidth;

	memset(m_BitsBuf,0,m_nBitsLen);
    if(m_Outline)
    {
        GreyBitType_Outline_Done(m_GBlibrary, m_Outline);
        m_Outline = NULL;
    }
    
    // 坐标变换，左上角为(0,0)
    m_Outline = GreyBitType_Outline_New(m_GBlibrary, slot->outline.n_contours, slot->outline.n_points);
    
    for(i=0;i<m_Outline->n_points;i++)
    {
        m_Outline->points[i].x =  (GB_Pos)(slot->outline.points[i].x-xStart);
#ifdef ENABLE_SCALE
        m_Outline->points[i].x = m_Outline->points[i].x*nCurrScale/100;
#endif
        m_Outline->points[i].y = (GB_Pos)-(slot->outline.points[i].y-yStart);
#ifdef ENABLE_SCALE
        m_Outline->points[i].y = nScaleYOffset+(m_Outline->points[i].y-nScaleYOffset)*nCurrScale/100;
#endif
        m_Outline->tags[i] = slot->outline.tags[i];
    }
    
    for(i=0;i<m_Outline->n_contours;i++)
    {
        m_Outline->contours[i] = slot->outline.contours[i];
    }
    
    m_nHoriOff = slot->metrics.horiBearingX>>6;
#ifdef ENABLE_SCALE
    m_nHoriOff = m_nHoriOff*nCurrScale/100;
#endif
    if(m_nHoriOff > 0)
    {
        m_nHoriOff = 0;
    }
    
    if(bShowOutline == 0)
    {
	    byte *pSrc, *pDst;
	    int yEnd = slot->bitmap.rows;
	    int xEnd = slot->bitmap.width;
	    pSrc = slot->bitmap.buffer;
        if(slot->metrics.horiBearingX < 0)
        {
            xStart = 0;
        }
        else
        {
            xStart = slot->metrics.horiBearingX>>6;
        }

        yStart = (yStart>>6)-slot->bitmap_top;
	    
        if(xStart<0)
        {
            xStart = 0;
        }

        if(yStart < 0)
        {
            yStart = 0;
        }

        pDst = m_BitsBuf+xStart+yStart*m_nWidth;
        if(slot->bitmap.pitch != slot->bitmap.width)
        {
            for(int y=0; y<yEnd; y++)
	        {
		        for(int x=0;x<xEnd;x++)
		        {
			        pDst[x] = (((*(pSrc + (x>>3))>>( 7-(x%8)))&0x1) == 0)?0:0xFF;
		        }
		        pSrc += slot->bitmap.pitch;
		        pDst += m_nWidth;
	        }
        }
        else
        {
	        for(int y=0; y<yEnd; y++)
	        {
		        for(int x=0;x<xEnd;x++)
		        {
			        pDst[x] = pSrc[x];
		        }
		        pSrc += slot->bitmap.pitch;
		        pDst += m_nWidth;
	        }
        }
    }
    else
    {
        GBHANDLE pRaster = GreyBit_Raster_New(m_GBlibrary, 0);
        if(pRaster)
        {
            GB_BitmapRec bitmap;
            bitmap.bitcount = 8;
            bitmap.buffer   = m_BitsBuf;
            bitmap.pitch    = m_nWidth;
            bitmap.width    = m_nWidth;
            bitmap.height   = m_nHeight;
            GreyBit_Raster_Render(pRaster, &bitmap, m_Outline);
            GreyBit_Raster_Done(pRaster);
        }
    }
	return error;
}
