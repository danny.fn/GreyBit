#include "GreyBitFontEngine.h"

GreyBitFontEngine::GreyBitFontEngine(void)
{
    m_Library  = GreyBitType_Init();
	m_Creator  = NULL;
	m_pFileNamePtr = NULL;
    m_Bitmap = NULL;
    m_Index = NULL;
}

GreyBitFontEngine::~GreyBitFontEngine(void)
{
    if(m_Bitmap)
    {
        GreyBitType_Bitmap_Done(m_Library, m_Bitmap);
    }

	if(m_Creator)
	{
		GreyBitType_Creator_Done( m_Creator );
	}
	GreyBitType_Done(m_Library);
}

bool GreyBitFontEngine::SetFile(TCHAR *pName)
{
	if(m_pFileNamePtr == pName)
	{
		return true;
	}

	m_pFileNamePtr = pName;
	if(m_Creator)
	{
		GreyBitType_Creator_Done( m_Creator );
        m_Creator = NULL;
	}
	// DBCS string
	char *pTemp;
	int nTxtTotalChar;

	//System.Text.UnicodeEncoding myUnicode;
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, NULL, 0, NULL, NULL);
	
	pTemp = new char[nTxtTotalChar];
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, pTemp, nTxtTotalChar, NULL, NULL);
	m_Creator = GreyBitType_Creator_New( m_Library, pTemp);
    m_Index = 0;
	delete pTemp;
    if(m_Creator == NULL)
    {
        m_pFileNamePtr = NULL;
        return false;
    }
    return true;
}

int GreyBitFontEngine::PrepareCreate(int nHeight, int nBitCount, bool bCompress)
{
    GreyBitType_Creator_SetParam(m_Creator, GB_PARAM_HEIGHT,    (GB_UINT32)nHeight);
    GreyBitType_Creator_SetParam(m_Creator, GB_PARAM_BITCOUNT,  (GB_UINT32)nBitCount);
    GreyBitType_Creator_SetParam(m_Creator, GB_PARAM_COMPRESS,  (GB_UINT32)bCompress);
    if(m_Bitmap)
    {
        GreyBitType_Bitmap_Done(m_Library, m_Bitmap);
    }
    m_Bitmap = GreyBitType_Bitmap_New(m_Library, nHeight<<1, nHeight, nBitCount, NULL);
    return 0;
}

int GreyBitFontEngine::SetBits(int nCode, byte *pBuff, int nWidth, int nBitCount, int nHoriOff)
{
    if(nWidth > m_Bitmap->height*3)
    {
        return -1;
    }

    if(m_Bitmap->width != nWidth)
    {
        m_Bitmap->width = nWidth;
        m_Bitmap->pitch	= (((m_Bitmap->width<<3)*m_Bitmap->bitcount)+63)>>6;
    }
    
    if(nBitCount == m_Bitmap->bitcount)
    {
        memcpy(m_Bitmap->buffer, pBuff, m_Bitmap->height*m_Bitmap->pitch);
    }
    else
    {
        if(nBitCount == 8) //To 1
        {
            //指向目标数据
            byte* pbyDst = m_Bitmap->buffer;
            byte* pbySrc = pBuff;
            int i,j;
            
            //完成变换
            for( i = 0; i < m_Bitmap->height;i++)
            {
                //取整
                for( j = 0;j < m_Bitmap->pitch;j++)
                {
                    pbyDst[j] = 0;
                }
                
                for( j = 0;j < m_Bitmap->width; j++)
                {
                    //获取数据
                    if(pbySrc[j] >= BITMAP8TO1_SWITCH_VALUE)
                    {
                        pbyDst[j>>3] |= (0x80)>>(j%8);
                    }
                }
                pbyDst += m_Bitmap->pitch;
                pbySrc += nWidth;
            }
        }
        else if(nBitCount == 1) //To 8
        {
            //指向目标数据
            byte* pbyDst = m_Bitmap->buffer;
            byte* pbySrc = pBuff;
            int nPitch	= (((nWidth<<3)*nBitCount)+63)>>6;
            int i,j;
            
            //完成变换
            for( i = 0; i < m_Bitmap->height;i++)
            {
                for( j = 0;j < m_Bitmap->width; j++)
                {
                    byte Mask = 0x80>>(j%8);
                    byte Curr = pbySrc[j>>3];
                    
                    if(Curr & Mask)
                    {
                        pbyDst[j] = 0xFF;
                    }
                    else
                    {
                        pbyDst[j] = 0x0;
                    }
                }
                pbyDst += m_Bitmap->pitch;
                pbySrc += nPitch;
            }
        }
        else
        {
            return -1;
        }
    }
    GB_DataRec data;
    data.data       = m_Bitmap;
    data.format     = GB_FORMAT_BITMAP;
    data.width      = nWidth;
    data.horioff    = nHoriOff;
    return GreyBitType_Creator_SaveChar(m_Creator, nCode, &data);
}


int GreyBitFontEngine::SetOutline(int nCode, byte *pBuff, int nWidth, int nHoriOff)
{
    if(nWidth > m_Bitmap->height*3)
    {
        return -1;
    }
    GB_DataRec data;
    data.data       = pBuff;
    data.format     = GB_FORMAT_OUTLINE;
    data.width      = nWidth;
    data.horioff    = nHoriOff;
    return GreyBitType_Creator_SaveChar(m_Creator, nCode, &data);
}

int GreyBitFontEngine::CombineFile(TCHAR *pName)
{
    // DBCS string
	char *pTemp;
    GBHANDLE loader;
	int nTxtTotalChar;
    int nRet;

	//System.Text.UnicodeEncoding myUnicode;
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, NULL, 0, NULL, NULL);
	
	pTemp = new char[nTxtTotalChar];
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, pTemp, nTxtTotalChar, NULL, NULL);
	loader = GreyBitType_Loader_New( m_Library, pTemp);
    
	delete pTemp;
    
    GB_DataRec data;
    data.data       = loader;
    data.format     = GB_FORMAT_STREAM;
    data.width      = 0;
    data.horioff    = 0;
    nRet = GreyBitType_Creator_SaveChar(m_Creator, m_Index, &data);
    m_Index++;
    return nRet;
}

int GreyBitFontEngine::FinishCreate(void)
{
    m_Index = 0;
    return GreyBitType_Creator_Flush(m_Creator);
}

//-----------------------------------------------------------------------------

GreyBitViewFontEngine::GreyBitViewFontEngine(void)
{
    m_Library  = GreyBitType_Init();
	m_Loader   = NULL;
    m_Layout   = NULL;
	m_nSize    = 0;
    m_nScale   = 100;
	m_BitsBuf  = NULL;
	m_nBufLen  = 0;
	m_nBitsLen = 0;
	m_nHeight  = 0;
	m_nWidth   = 0;
	m_pFileNamePtr = NULL;
    m_Bitmap   = 0;
}

GreyBitViewFontEngine::~GreyBitViewFontEngine(void)
{
	if(m_BitsBuf)
	{
		delete [] m_BitsBuf;
	}

    if(m_Bitmap)
    {
        GreyBitType_Bitmap_Done(m_Library, m_Bitmap);
    }

	if(m_Layout)
	{
		GreyBitType_Layout_Done( m_Layout );
	}
    if(m_Loader)
	{
		GreyBitType_Loader_Done( m_Loader );
	}
	GreyBitType_Done(m_Library);
}

bool GreyBitViewFontEngine::SetFile(TCHAR *pName)
{
	if(m_pFileNamePtr == pName)
	{
		return true;
	}

	m_pFileNamePtr = pName;
	if(m_Loader)
	{
		GreyBitType_Loader_Done( m_Loader );
        m_Loader = NULL;
	}
	// DBCS string
	char *pTemp;
	int nTxtTotalChar;

	//System.Text.UnicodeEncoding myUnicode;
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, NULL, 0, NULL, NULL);
	
	pTemp = new char[nTxtTotalChar];
	nTxtTotalChar = WideCharToMultiByte(CP_ACP, 0, pName, -1, pTemp, nTxtTotalChar, NULL, NULL);
	m_Loader = GreyBitType_Loader_New( m_Library, pTemp);

	int nNewSize = m_nSize;
	m_nSize = 0;
	SetFontSize(nNewSize, m_nScale);
	delete pTemp;
    if(m_Loader == NULL)
    {
        m_pFileNamePtr = NULL;
        return false;
    }
    
    GreyBitType_Loader_SetParam(m_Loader, GB_PARAM_CACHEITEM, 100);
    return true;
}

void GreyBitViewFontEngine::SetFontSize(int nSize, int nScale)
{
    m_nScale = nScale;
	if(m_nSize != nSize && m_Loader && nSize)
	{
		m_nSize = nSize;
        if(m_Layout)
	    {
		    GreyBitType_Layout_Done( m_Layout );
            m_Layout = NULL;
	    }

        m_Layout = GreyBitType_Layout_New( m_Loader, m_nSize, 8, 0, 0);

		if(m_BitsBuf)
		{
			delete [] m_BitsBuf;
		}
		m_nBufLen = m_nSize*m_nSize*3;
		m_BitsBuf = new byte[m_nBufLen];
	}
}

bool GreyBitViewFontEngine::IsCodeAvaliable(int nCode)
{
	return (GreyBitType_Loader_IsExist(m_Loader, nCode) != 0);
}

int GreyBitViewFontEngine::LoadGlyph(int nCode,char bShowOutline)
{
	int error = -1;
	GB_Bitmap  bitmap;
    error = GreyBitType_Layout_LoadChar(m_Layout, nCode, &bitmap);
    
    if(error == 0)
    {
        m_nWidth   = GreyBitType_Layout_GetWidth(m_Layout, nCode);
	    m_nWidth   = bitmap->width;
	    m_nHeight  = bitmap->height;
	    m_nBitsLen = m_nHeight*bitmap->pitch;
    	m_nHoriOff = bitmap->horioff;

        memcpy(m_BitsBuf, bitmap->buffer, m_nBitsLen);
    }
	return error;
}

byte *GreyBitViewFontEngine::GetBits(void)
{
	return m_BitsBuf;
}
