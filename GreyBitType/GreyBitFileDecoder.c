#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYBITFILE
#include "GreyBitFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
// Compress Function
static int GreyBitFile_Decoder_Decompress(GB_BYTE *pOutData, GB_INT32 *pnInOutLen, GB_BYTE *pInData, GB_INT32 nInDataLen);

static int GreyBitFile_Decoder_Init(GBF_Decoder me);
static int GreyBitFile_Decoder_ReadHeader(GBF_Decoder me);
static int GreyBitFile_Decoder_InfoInit(GBF_Decoder me, GB_INT16 nMaxWidth, GB_INT16 nHeight, GB_INT16 nBitCount, GB_INT16 bCompress);

static GBF_Offset GreyBitFile_Decoder_GetDataOffset(GBF_Decoder me, GB_UINT32 nCode);
static int  GreyBitFile_Decoder_CaheItem(GBF_Decoder me, GB_UINT32 nCode, GB_BYTE *pData, GB_INT32 nDataSize);
static void GreyBitFile_Decoder_ClearCache(GBF_Decoder me);
static GB_INT16 GreyBitFile_Decoder_GetHoriOff(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);

//-----------------------------------------------------------------------------
// Decoder
//-----------------------------------------------------------------------------
GB_Decoder GreyBitFile_Decoder_New(GB_Loader loader, GB_Stream stream)
{
    GBF_Decoder decoder = (GBF_Decoder)GreyBit_Malloc(loader->gbMem,sizeof(GBF_DecoderRec));
    if(decoder)
    {
        decoder->gbDecoder.setparam     = GreyBitFile_Decoder_SetParam;
        decoder->gbDecoder.getcount     = GreyBitFile_Decoder_GetCount;
        decoder->gbDecoder.getheight    = GreyBitFile_Decoder_GetHeight;
        decoder->gbDecoder.getwidth     = GreyBitFile_Decoder_GetWidth;
        decoder->gbDecoder.getadvance   = GreyBitFile_Decoder_GetAdvance;
        decoder->gbDecoder.decode       = GreyBitFile_Decoder_Decode;
        decoder->gbDecoder.done         = GreyBitFile_Decoder_Done;
        decoder->gbLibrary              = loader->gbLibrary;
        decoder->gbMem                  = loader->gbMem;
        decoder->gbStream               = stream;
        decoder->nCacheItem	            = 0;
        decoder->nItemCount             = 0;
        decoder->gbOffDataBits          = sizeof(GREYBITFILEHEADER)+sizeof(GREYBITINFOHEADER);
        GreyBitFile_Decoder_Init(decoder);
    }
    return (GB_Decoder)decoder;
}

int GreyBitFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    switch(nParam){
    case GB_PARAM_CACHEITEM:
        if(dwParam)
        {
            // Cache Size只能设置一次
            if(me->gpGreyBits)
            {
                return -1;
            }
            me->nCacheItem = dwParam;
            me->gpGreyBits = (GB_BYTE **)GreyBit_Malloc(me->gbMem, me->nCacheItem*sizeof(GB_BYTE *));
            me->pnGreySize = (GBF_Lenght *)GreyBit_Malloc(me->gbMem, me->nCacheItem*sizeof(GBF_Lenght));
            me->nGreyBitsCount = 0;
        }
        break;
        
    default:
        return -1;
    }
    return 0;
}

GB_INT32 GreyBitFile_Decoder_GetCount(GB_Decoder decoder)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    return me->nItemCount;
}

GB_INT32 GreyBitFile_Decoder_GetHeight(GB_Decoder decoder)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    return me->gbInfoHeader.gbiHeight;
}

GB_INT16 GreyBitFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    GBF_Index WidthIdx;
    GB_INT32 UniIndex;
    GB_UINT16 nMinCode;
    GBF_Width nWidth;

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return GB_WIDTH_DEFAULT;
    }

    WidthIdx = me->gbInfoHeader.gbiWidthSection.gbSectionOff[UniIndex];
    if(WidthIdx == 0)
    {
        return GB_WIDTH_DEFAULT;
    }

    WidthIdx--;
    if(me->gbWidthTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        nWidth = me->gbWidthTable[WidthIdx+(nCode-nMinCode)];
    }
    else
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        WidthIdx += (GBF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(WidthIdx*sizeof(GBF_Width)+me->gbOffDataBits+me->gbInfoHeader.gbiWidthTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)&nWidth, sizeof(GBF_Width));
    }
    
    return nWidth*nSize/me->gbInfoHeader.gbiHeight;;
}

GB_INT16 GreyBitFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GB_INT16 nAdvance;
    GB_INT16 nWidth = GreyBitFile_Decoder_GetWidth(decoder, nCode, nSize);
    GB_INT16 nHoriOff = GreyBitFile_Decoder_GetHoriOff(decoder, nCode, nSize);
    
    nAdvance = nWidth+nHoriOff;
    return (nAdvance>0)?nAdvance:0;
}

int	 GreyBitFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    GBF_Offset Offset = GreyBitFile_Decoder_GetDataOffset(me, nCode);
    GB_BYTE *pByteData;
    GB_INT32 nDataLen, nInDataLen;
    GB_INT16 nWidth = GreyBitFile_Decoder_GetWidth(decoder, nCode, me->gbInfoHeader.gbiHeight);
    GB_INT16 nHoriOff = GreyBitFile_Decoder_GetHoriOff(decoder, nCode, nSize);
    GBF_Lenght Lenght;

    if(nWidth == 0)
    {
        return -1;
    }
    
    me->gbBitmap->width = (GB_INT16)nWidth;
    me->gbBitmap->pitch = (GB_INT16)(((nWidth<<3)*me->gbBitmap->bitcount)+63)>>6;
    me->gbBitmap->horioff = nHoriOff;
    nDataLen = me->gbBitmap->height*me->gbBitmap->pitch;

    if(IS_INRAM(Offset))
    {
        Offset = GET_INDEX(Offset);
        pByteData = me->gpGreyBits[Offset];
        nInDataLen = me->pnGreySize[Offset];
    }
    else
    {
        // 从文件中读取
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(Offset+me->gbOffDataBits+me->gbInfoHeader.gbiOffGreyBits));
        if(me->gbInfoHeader.gbiCompression && me->gbInfoHeader.gbiBitCount == 8)
        {
            GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&Lenght, sizeof(GBF_Lenght));
            nInDataLen = Lenght;
        }
        else
        {
            nInDataLen = nDataLen;
        }
        GreyBit_Stream_Read(me->gbStream, me->pBuff, nInDataLen);
        pByteData = me->pBuff;
        GreyBitFile_Decoder_CaheItem(me, nCode, pByteData, nInDataLen);
    }

    if(pByteData)
    {
        if(me->gbInfoHeader.gbiCompression)
        {
            GreyBitFile_Decoder_Decompress(me->gbBitmap->buffer, &nDataLen, pByteData, nInDataLen);
        }
        else
        {
            GB_MEMCPY(me->gbBitmap->buffer, pByteData, nDataLen);
        }

        if(pData)
        {
            pData->format = GB_FORMAT_BITMAP;
            pData->data   = me->gbBitmap;
            pData->width  = nWidth;
        }
    }
    else
    {
        return -1;
    }
    return 0;
}

void GreyBitFile_Decoder_Done(GB_Decoder decoder)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    GreyBitFile_Decoder_ClearCache(me);
    GreyBit_Free(me->gbMem,decoder);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
static int GreyBitFile_Decoder_Decompress(GB_BYTE *pOutData, GB_INT32 *pnInOutLen, GB_BYTE *pInData, GB_INT32 nInDataLen)
{
    GB_BYTE nLen, nData;
    GB_INT32 i,j;
    GB_INT32 nOutlLen = *pnInOutLen;
    GB_INT32 nDecompressLen = 0;
    
    nLen = 0;
    if(pOutData)
    {
        for(i=0;i<nInDataLen;i++)
        {
            nData = pInData[i];
            if(nLen == 0)
            {
                if(IS_LEN(nData))
                {
                    nLen = GET_LEN(nData);
                }
                else
                {
                    pOutData[nDecompressLen] = (nData<<1)|0x1;
                    nDecompressLen++;
                }
            }
            else
            {
                for(j=0;j<nLen;j++)
                {
                    pOutData[nDecompressLen] = (nData<<1)|0x1;
                    nDecompressLen++;
                }
                nLen = 0;
            }
        }
    }
    else
    {
        // 计算长度
        for(i=0;i<nInDataLen;i++)
        {
            nData = pInData[i];
            if(nLen == 0)
            {
                if(IS_LEN(nData))
                {
                    nLen = GET_LEN(nData);
                }
                else
                {
                    nDecompressLen++;
                }
            }
            else
            {
                for(j=0;j<nLen;j++)
                {
                    nDecompressLen++;
                }
                nLen = 0;
            }
        }
    }
    if(nLen != 0)
    {
        return -1;
    }
    *pnInOutLen = nDecompressLen;
    return 0;
}

static GBF_Offset GreyBitFile_Decoder_GetDataOffset(GBF_Decoder me, GB_UINT32 nCode)
{
    GB_INT32 UniIndex;
    GBF_Index  SectionIndex;
    GB_UINT16 nMinCode;
    
    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return 0;
    }

    SectionIndex = me->gbInfoHeader.gbiIndexSection.gbSectionOff[UniIndex];
    if(SectionIndex == 0)
    {
        return 0;
    }
    
    SectionIndex--;
    if(me->gbOffsetTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        return me->gbOffsetTable[SectionIndex+(nCode-nMinCode)];
    }
    else
    {
        GBF_Offset nOffset;
        
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        SectionIndex += (GBF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(SectionIndex*sizeof(GBF_Offset)+me->gbOffDataBits+me->gbInfoHeader.gbiOffsetTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&nOffset, sizeof(GBF_Offset));
        return nOffset;
    }
    return 0;
}

static int GreyBitFile_Decoder_CaheItem(GBF_Decoder me, GB_UINT32 nCode, GB_BYTE *pData, GB_INT32 nDataSize)
{
    GB_INT32 UniIndex;
    GBF_Index SectionIndex;
    GB_UINT16 nMinCode;

    if(me->nGreyBitsCount >= me->nCacheItem || me->gbOffsetTable == 0)
    {
        return -1;
    }

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return -1;
    }

    SectionIndex = me->gbInfoHeader.gbiIndexSection.gbSectionOff[UniIndex];
    if(SectionIndex == 0)
    {
        return -1;
    }
    
    me->gpGreyBits[me->nGreyBitsCount] = (GB_BYTE *)GreyBit_Malloc(me->gbMem, nDataSize);
    me->pnGreySize[me->nGreyBitsCount] = (GBF_Lenght)nDataSize;
    GB_MEMCPY(me->gpGreyBits[me->nGreyBitsCount], pData, nDataSize);
    
    // 标志Index Cache
    
    UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
    SectionIndex--;
    SectionIndex += (GBF_Index)(nCode-nMinCode);
    me->gbOffsetTable[SectionIndex] = SET_RAM(me->nGreyBitsCount);
    me->nGreyBitsCount++;
    return 0;
}

static void GreyBitFile_Decoder_ClearCache(GBF_Decoder me)
{
    // Bitmap
    if(me->gbBitmap)
    {
        GreyBitType_Bitmap_Done(me->gbLibrary, me->gbBitmap);
    }

    if(me->pBuff)
    {
        GreyBit_Free(me->gbMem,me->pBuff);
    }

    if(me->gbWidthTable)
    {
        GreyBit_Free(me->gbMem,me->gbWidthTable);
    }
    
    if(me->gbHoriOffTable)
    {
        GreyBit_Free(me->gbMem,me->gbHoriOffTable);
    }

    if(me->gbOffsetTable)
    {
        GreyBit_Free(me->gbMem,me->gbOffsetTable);
    }

    if(me->pnGreySize)
    {
        GreyBit_Free(me->gbMem,me->pnGreySize);
    }

    if(me->gpGreyBits)
    {
        GB_INT32 i;
        for(i=0;i<me->nCacheItem;i++)
        {
            if(me->gpGreyBits[i])
            {
                GreyBit_Free(me->gbMem, me->gpGreyBits[i]);
            }
        }
        GreyBit_Free(me->gbMem,me->gpGreyBits);
    }
}

static int GreyBitFile_Decoder_Init(GBF_Decoder me)
{
    int nRet;
    int nDataSize;

    nRet = GreyBitFile_Decoder_ReadHeader(me);
    if(nRet < 0)
    {
        return nRet;
    }

    // Cache Width
    nDataSize = me->gbInfoHeader.gbiHoriOffTabOff-me->gbInfoHeader.gbiWidthTabOff;
    me->gbWidthTable = (GBF_Width *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiWidthTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)me->gbWidthTable, nDataSize);

    // Hori Offset
    nDataSize = me->gbInfoHeader.gbiOffsetTabOff-me->gbInfoHeader.gbiHoriOffTabOff;
    me->gbHoriOffTable = (GBF_Width *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiHoriOffTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)me->gbHoriOffTable, nDataSize);

    // Cache Index
    nDataSize = me->gbInfoHeader.gbiOffGreyBits-me->gbInfoHeader.gbiOffsetTabOff;
    me->gbOffsetTable = (GBF_Offset *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiOffsetTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)me->gbOffsetTable, nDataSize);
    return 0;
}

static int GreyBitFile_Decoder_ReadHeader(GBF_Decoder me)
{
    GreyBit_Stream_Seek(me->gbStream, 0);
    if(GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYBITFILEHEADER)) != sizeof(GREYBITFILEHEADER))
    {
        return -1;
    }

    GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&me->gbInfoHeader, sizeof(GREYBITINFOHEADER));
    me->nItemCount = me->gbInfoHeader.gbiCount;
    GreyBitFile_Decoder_InfoInit(me, me->gbInfoHeader.gbiWidth, me->gbInfoHeader.gbiHeight, me->gbInfoHeader.gbiBitCount, me->gbInfoHeader.gbiCompression);
    return 0;
}

static int GreyBitFile_Decoder_InfoInit(GBF_Decoder me, GB_INT16 nMaxWidth, GB_INT16 nHeight, GB_INT16 nBitCount, GB_INT16 bCompress)
{
    me->gbBitmap = GreyBitType_Bitmap_New(me->gbLibrary, nMaxWidth, nHeight, nBitCount, 0);
    me->nBuffSize= me->gbBitmap->height*me->gbBitmap->pitch;
    me->pBuff    = (GB_BYTE *)GreyBit_Malloc(me->gbMem, me->nBuffSize);
    return 0;
}

static GB_INT16 GreyBitFile_Decoder_GetHoriOff(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GBF_Decoder me = (GBF_Decoder)decoder;
    GBF_Index   HoriOffIdx;
    GB_INT32    UniIndex;
    GB_UINT16   nMinCode;
    GBF_HoriOff nHoriOff;

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return GB_HORIOFF_DEFAULT;
    }

    HoriOffIdx = me->gbInfoHeader.gbiWidthSection.gbSectionOff[UniIndex];
    if(HoriOffIdx == 0)
    {
        return GB_HORIOFF_DEFAULT;
    }

    HoriOffIdx--;
    if(me->gbHoriOffTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        nHoriOff = me->gbHoriOffTable[HoriOffIdx+(nCode-nMinCode)];
    }
    else
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        HoriOffIdx += (GBF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(HoriOffIdx*sizeof(GBF_HoriOff)+me->gbOffDataBits+me->gbInfoHeader.gbiHoriOffTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)&nHoriOff, sizeof(GBF_HoriOff));
    }
    nHoriOff = nHoriOff*nSize/me->gbInfoHeader.gbiHeight;
    return nHoriOff;
}
#endif

