#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYVECTORFILE
#include "GreyVectorFile.h"

//-----------------------------------------------------------------------------
// Format
//-----------------------------------------------------------------------------
GB_BOOL GreyVectorFile_Probe(GB_Stream stream)
{
    GREYVECTORFILEHEADER fileHeader;
    GreyBit_Stream_Seek(stream, 0);
    if(GreyBit_Stream_Read(stream, (GB_BYTE *)&fileHeader, sizeof(GREYVECTORFILEHEADER)) == sizeof(GREYVECTORFILEHEADER))
    {
        if( fileHeader.gbfTag[0] == 'g' 
          &&fileHeader.gbfTag[1] == 'v'
          &&fileHeader.gbfTag[2] == 't' 
          &&fileHeader.gbfTag[3] == 'f' )
        {
            return GB_TRUE;
        }
    }
    return GB_FALSE;
}

GB_Format GreyVectorFile_Format_New(GB_Library library)
{
    GB_Format format = (GB_Format)GreyBit_Malloc(library->gbMem,sizeof(GB_FormatRec));
    if(format)
    {
        format->next		= 0;
        format->tag[0]		= 'g';
        format->tag[1]		= 'v';
        format->tag[2]		= 'f';
        format->probe		= GreyVectorFile_Probe;
        format->decodernew  = GreyVectorFile_Decoder_New;
#ifdef ENABLE_ENCODER
        format->encodernew  = GreyVectorFile_Encoder_New;
#endif
    }
    return format;
}
#endif

