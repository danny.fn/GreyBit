#ifndef GREYCOMBINEFILE_H_
#define GREYCOMBINEFILE_H_
#include "GreyBitType.h"
#include "UnicodeSection.h"
#include "GreyBitType_Def.h"

#ifdef __cplusplus
extern "C"{
#endif

#define GCF_ITEM_MAX    5
#define GCF_BUFF_SIZE   1024

typedef struct tagGREYCOMBINEITEMINFO {
    GB_UINT32           gbiHeight;          // 字符高度，0为矢量字体
    GB_UINT32           gbiDataOff;         // 文件偏移
    GB_UINT32           gbiDataSize;        // 数据区大小
} GREYCOMBINEITEMINFO, *PGREYCOMBINEITEMINFO;

// FileHeader+InfoHeader+SectionTable(Width)+SectionTable(Index)+Data(GreyBits)
typedef struct tagGREYCOMBINEFILEHDR {
    char                gbfTag[4];    // gctf
    GREYCOMBINEITEMINFO gbfInfo[GCF_ITEM_MAX];
} GREYCOMBINEFILEHDR, *PGREYCOMBINEFILEHDR;

// Decoder
typedef struct _GCF_DecoderRec{
    GB_DecoderRec       gbDecoder;
    GB_Library          gbLibrary;
    GB_Memory           gbMem;
    GB_Stream           gbStream;
    GREYCOMBINEFILEHDR  gbFileHeader;
    GBHANDLE            gbLoader[GCF_ITEM_MAX];
}GCF_DecoderRec,*GCF_Decoder;

extern GB_Decoder   GreyCombineFile_Decoder_New(GB_Loader loader, GB_Stream stream);
extern int          GreyCombineFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam);
extern GB_INT32     GreyCombineFile_Decoder_GetCount(GB_Decoder decoder);
extern GB_INT32     GreyCombineFile_Decoder_GetHeight(GB_Decoder decoder);
extern GB_INT16     GreyCombineFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern GB_INT16     GreyCombineFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern int          GreyCombineFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize);
extern void         GreyCombineFile_Decoder_Done(GB_Decoder decoder);

#ifdef ENABLE_ENCODER
typedef struct _GCF_EncoderRec{
    GB_EncoderRec       gbEncoder;
    GB_Library          gbLibrary;
    GB_Memory           gbMem;
    GB_Stream           gbStream;
    GREYCOMBINEFILEHDR  gbFileHeader;
    GB_Loader           gbItemLoader[GCF_ITEM_MAX];
    GB_BYTE             gbBuffer[GCF_BUFF_SIZE];
}GCF_EncoderRec,*GCF_Encoder;

extern GB_Encoder   GreyCombineFile_Encoder_New(GB_Creator creator, GB_Stream stream);
extern GB_INT32     GreyCombineFile_Encoder_GetCount(GB_Encoder encoder);
extern int          GreyCombineFile_Encoder_SetParam(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam);
extern int          GreyCombineFile_Encoder_Delete(GB_Encoder encoder, GB_UINT32 nCode);
extern int          GreyCombineFile_Encoder_Encode(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData);
extern int          GreyCombineFile_Encoder_Flush(GB_Encoder encoder);
extern void         GreyCombineFile_Encoder_Done(GB_Encoder encoder);
#endif
#ifdef __cplusplus
}
#endif 
#endif