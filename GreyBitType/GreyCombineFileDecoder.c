#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYCOMBINEFILE
#include "GreyCombineFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
static int GreyCombineFile_Decoder_Init(GCF_Decoder me);

//-----------------------------------------------------------------------------
// Decoder
//-----------------------------------------------------------------------------
GB_Decoder GreyCombineFile_Decoder_New(GB_Loader loader, GB_Stream stream)
{
    GCF_Decoder decoder = (GCF_Decoder)GreyBit_Malloc(loader->gbMem,sizeof(GCF_DecoderRec));
    if(decoder)
    {
        decoder->gbDecoder.setparam     = GreyCombineFile_Decoder_SetParam;
        decoder->gbDecoder.getcount     = GreyCombineFile_Decoder_GetCount;
        decoder->gbDecoder.getheight    = GreyCombineFile_Decoder_GetHeight;
        decoder->gbDecoder.getwidth     = GreyCombineFile_Decoder_GetWidth;
        decoder->gbDecoder.getadvance   = GreyCombineFile_Decoder_GetAdvance;
        decoder->gbDecoder.decode       = GreyCombineFile_Decoder_Decode;
        decoder->gbDecoder.done         = GreyCombineFile_Decoder_Done;
        decoder->gbLibrary              = loader->gbLibrary;
        decoder->gbMem                  = loader->gbMem;
        decoder->gbStream               = stream;
        GB_MEMSET(decoder->gbLoader, 0, sizeof(decoder->gbLoader));
        GreyCombineFile_Decoder_Init(decoder);
    }
    return (GB_Decoder)decoder;
}

int GreyCombineFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    GB_Loader loader;
    int i;
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader)
        {
            GreyBit_Decoder_SetParam(loader->gbDecoder, nParam, dwParam);
        }
    }
    return 0;
}

GB_INT32 GreyCombineFile_Decoder_GetCount(GB_Decoder decoder)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    GB_Loader loader;
    int i;
    GB_INT32 nCount = 0;
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader)
        {
            nCount += GreyBit_Decoder_GetCount(loader->gbDecoder);
        }
    }
    return nCount;
}

GB_INT32 GreyCombineFile_Decoder_GetHeight(GB_Decoder decoder)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    return 0;
}

GB_INT16 GreyCombineFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    GB_Loader loader;
    int i;
    
    // 优先匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == nSize)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetWidth(loader->gbDecoder, nCode, nSize);
            }
        }
    }
    
    // 使用矢量字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetWidth(loader->gbDecoder, nCode, nSize);
            }
        }
    }

    // 矢量字库中没有则再次匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight != 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetWidth(loader->gbDecoder, nCode, nSize);
            }
        }
    }
    return 0;
}

GB_INT16 GreyCombineFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    GB_Loader loader;
    int i;
    
    // 优先匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == nSize)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetAdvance(loader->gbDecoder, nCode, nSize);
            }
        }
    }
    
    // 使用矢量字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetAdvance(loader->gbDecoder, nCode, nSize);
            }
        }
    }
    
    // 矢量字库中没有则再次匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight != 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return (GB_INT16)GreyBit_Decoder_GetAdvance(loader->gbDecoder, nCode, nSize);
            }
        }
    }
    return 0;
}

int	 GreyCombineFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    GB_Loader loader;
    int i;
    
    // 优先匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == nSize)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return GreyBit_Decoder_Decode(loader->gbDecoder, nCode, pData, nSize);
            }
        }
    }
    
    // 使用矢量字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight == 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return GreyBit_Decoder_Decode(loader->gbDecoder, nCode, pData, nSize);
            }
        }
    }
    
    // 矢量字库中没有则再次匹配点阵字库
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        loader = (GB_Loader)me->gbLoader[i];
        if(loader && me->gbFileHeader.gbfInfo[i].gbiHeight != 0)
        {
            if(GreyBitType_Loader_IsExist(loader, nCode))
            {
                return GreyBit_Decoder_Decode(loader->gbDecoder, nCode, pData, nSize);
            }
        }
    }
    return -1;
}

void GreyCombineFile_Decoder_Done(GB_Decoder decoder)
{
    GCF_Decoder me = (GCF_Decoder)decoder;
    int i;
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbLoader[i])
        {
            GreyBitType_Loader_Done(me->gbLoader[i]);
            me->gbLoader[i] = 0;
        }
    }
    GreyBit_Free(me->gbMem,decoder);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
static int GreyCombineFile_Decoder_Init(GCF_Decoder me)
{
    GB_INT32 nPreOffset, nOffset, nSize;
    int i;
    
    nSize   = me->gbStream->size;
    nPreOffset = nOffset = sizeof(GREYCOMBINEFILEHDR);
    GreyBit_Stream_Seek(me->gbStream, 0);
    if(GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYCOMBINEFILEHDR)) != sizeof(GREYCOMBINEFILEHDR))
    {
        return -1;
    }
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbFileHeader.gbfInfo[i].gbiDataOff)
        {
            GreyBit_Stream_Offset(me->gbStream, me->gbFileHeader.gbfInfo[i].gbiDataOff, 0);
            me->gbLoader[i] = GreyBitType_Loader_New_Stream(me->gbLibrary, me->gbStream, me->gbFileHeader.gbfInfo[i].gbiDataSize);
        }
    }
    return 0;
}

#endif

