SHELL=sh.exe
#============================================================================
#  Name:
#    makefile
#
#  Description:
#    Makefile to build the $(TARGET) module.
#
#   The following nmake targets are available in this makefile:
#
#     all           - make .elf and .mod image files (default)
#     clean         - delete object directory and image files
#     filename.o    - make object file
#     filename.mix  - make C and ASM mix file
#
#   The above targets can be made with the following command:
#
#     make [target]
#
#  Assumptions:
#    1. The ARM ADS 1.0.1 or higher tools are installed
#    2. The run environment of make toos is exist in this OS
#
#============================================================================
OBJSDIR       = ads12#           # Middle object directory
MODULE        = a#            # Binary module extension

#----------------------------------------------------------------------------
# Default target
#----------------------------------------------------------------------------
.PHONY : all
all : startup exe complete

include objs.min

#----------------------------------------------------------------------------
# Default SUFFIX rules
#----------------------------------------------------------------------------
OBJ_FILE = $(OBJ_CMD) $(@)#         # Output object file specification

.SUFFIXES :
.SUFFIXES : .o .c .cpp .s .mix .dep .depp .osm .min .mak .ext

#----------------------------------------------------------------------------
# Compiler rules
#----------------------------------------------------------------------------

include $(TOOLS_PATH)/ads12_lib.min

#----------------------------------------------------------------------------
# All target
#----------------------------------------------------------------------------
.PHONY : startup
startup:$(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ---------------------------------------------------------------
	@echo Compile startup
	@echo ---------------------------------------------------------------
	
.PHONY : exe
exe: $(TARGETDIR)/$(TARGET).$(MODULE)

.PHONY : complete
complete:
	@echo ---------------------------------------------------------------
	@echo All have been done
	@echo ---------------------------------------------------------------
	
#----------------------------------------------------------------------------
# Clean target
#----------------------------------------------------------------------------

# The object subdirectory, target image file, and target hex file are deleted.
.PHONY : clean
clean :
	@echo ---------------------------------------------------------------
	@echo CLEAN
	-rm -f -r $(OBJSDIR)
	-rm -f -r $(TARGETDIR)/$(TARGET).$(MODULE)
	@echo ---------------------------------------------------------------
	
#----------------------------------------------------------------------------
# Directory targets
#----------------------------------------------------------------------------
$(OBJSDIR)/exist:
	@echo ---------------------------------------------------------------
	@echo Creating $(@D) Directory
	@mkdir -p $(@D)
	@echo Building $(@D) > $(@D)/exist
	@echo ---------------------------------------------------------------

$(TARGETDIR)/exist:
	@echo ---------------------------------------------------------------
	@echo Creating $(@D) Directory
	@mkdir -p $(@D)
	@echo Building $(@D) > $(@D)/exist
	@echo ---------------------------------------------------------------
	
#============================================================================
